﻿angular.module("umbraco")
    .controller("A8ClearCacheController", function ($scope, notificationsService, A8ClearCacheResource) {
        $scope.loaded = true;

        $scope.clearCache = function () {
            A8ClearCacheResource.clearCache().then(function (response) {
                if (response.data == "true") {
                    notificationsService.success("Clear cache", "Cleared!");
                }
                else {
                    notificationsService.error("Clear cache", "Failed!");
                }
            });
        };
    });