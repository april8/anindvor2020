﻿angular.module("umbraco.resources")
    .factory("A8ClearCacheResource", function ($http) {
        return {
            clearCache: function () {
                return $http.post("backoffice/A8ClearCache/A8ClearCacheApi/ClearCache");
            },
        };
    });
