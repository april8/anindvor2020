﻿var extScope;

angular.module("umbraco").controller("A8.HtmlTableFormatterController", function ($scope, $timeout, assetsService) {

    extScope = $scope;

    // default template for new renderer
    var tableTemplate = '' +
        '<table>' +
        '<tr>' +
        '<td tabindex=1>50%</td>' +
        '<td tabindex=1>50%</td>' +
        '</tr>' +
        '</table>';

    // attempt to get unique id for dom elements for work with jqLite (TODO: find umbraco way)
    $scope.timestamp = Math.floor(Date.now() / 1000);

    // not in use
    var stripScripts = function (a, b, c) {
        b = new Option;
        b.innerHTML = a;
        for (a = b.getElementsByTagName('script'); c = a[0];) {
            c.parentNode.removeChild(c);
        }
        return b.innerHTML;
    }

    // tell the assetsService to load the libs
    assetsService
        .load([
            "/App_Plugins/A8HtmlTableFormatter/tableFormatter.js"
        ])
        .then(function () {

            setTimeout(function () {

                angular.element("#a8TableFormatterWrapper-" + $scope.timestamp).A8TableFormatter();

                // set table values
                if ($scope.model.value === null || $scope.model.value === "") {
                    $scope.model.value = tableTemplate;
                    angular.element("#a8TableFormatterContent-" + $scope.timestamp).html(tableTemplate);
                }
                else {
                    angular.element("#a8TableFormatterContent-" + $scope.timestamp).html($scope.model.value);
                }

                // get table values 
                $scope.$on("formSubmitting", function (e, args) {
                    $scope.model.value = angular.element("#a8TableFormatterContent-" + $scope.timestamp).html();;
                });

            }, 0);

        });
});
