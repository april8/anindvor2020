﻿angular.module('umbraco.resources').factory('a8DictionaryPickerMultiResource',
	function ($q, $http, umbRequestHelper)
	{
		return {
			getValues: function (requestedParentDictionaryKeyword)
			{
				return umbRequestHelper.resourcePromise(
					$http.get("backoffice/A8/A8DictionaryPickerApi/GetValues?requestedParentDictionaryKeyword=" + requestedParentDictionaryKeyword)
				);
			}
		};
	}
);