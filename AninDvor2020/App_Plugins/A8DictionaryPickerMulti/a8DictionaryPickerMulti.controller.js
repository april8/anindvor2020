﻿angular.module("umbraco")
	.controller("A8.A8DictionaryPickerControllerMulti", function ($scope, a8DictionaryPickerMultiResource)
	{
		a8DictionaryPickerMultiResource.getValues($scope.model.config.requestedParentDictionaryKeyword).then(function (response)
		{
			$scope.options = response;

			$scope.isSelected = function (value)
			{
				try
				{
					return $scope.model.value.some(x => x === value);
				}
				catch (err)
				{
					return false;
				}
			}
		});
	});