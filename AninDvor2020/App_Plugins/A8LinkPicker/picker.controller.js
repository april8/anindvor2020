﻿angular.module("umbraco").controller("A8.LinkPickerController", function ($scope, assetsService) {

    var ngi = angular.element('body').injector();
    var uDialogService = ngi.get('dialogService');

    // choose internal link
    $scope.chooseLink = function () {
        $scope.model.value = null;
        uDialogService.open({
            template: '../App_Plugins/A8LinkPicker/Dialogs/linkpicker.html',
            show: true,
            dialogData: $scope.model.config,
            callback: function (e) {
                // set model
                $scope.model.value = {};
                var _url = e.url || '';
                if (_url.length > 0) {
                    var _id = e.id || 0;
                    if (_id > 0) {
                        _url = _url.replace('https://', '').replace('http://', '').replace(location.host, "")
                    }
                    $scope.model.value = {
                        id: _id,
                        name: ($scope.model.config.hideTitleField == '1' && _id == 0 ? "" : (e.name || '')),
                        url: _url,
                        target: e.target || '_self',
                        querystring: e.querystring || ''
                    };
                }
                // close dialog
                uDialogService.close();
            }
        });
    };

    $scope.editLink = function () {
        var linkPickerModel = angular.copy($scope.model);
        uDialogService.open({
            template: '../App_Plugins/A8LinkPicker/Dialogs/linkpicker.html',
            show: true,
            dialogData: linkPickerModel.config,
            target: linkPickerModel.value,
            callback: function (e) {
                // set model
                $scope.model.value = {};
                var _url = e.url || '';
                if (_url.length > 0) {
                    var _id = e.id || 0;
                    if (_id > 0) {
                        _url = _url.replace('https://', '').replace('http://', '').replace(location.host, "")
                    }
                    $scope.model.value = {
                        id: _id,
                        name: ($scope.model.config.hideTitleField == '1' && _id == 0 ? "" : (e.name || '')),
                        url: _url,
                        target: e.target || '_self',
                        querystring: e.querystring || ''
                    };
                }
                // close dialog
                uDialogService.close();
            }
        });
    };

    // remove link
    $scope.removeLink = function () {
        $scope.model.value = null;
    };

});