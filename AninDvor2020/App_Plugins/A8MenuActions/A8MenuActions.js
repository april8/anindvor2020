﻿function A8MenuShowHide(id) {
    $.post('/umbraco/api/MenuApi/ShowHide/?id=' + id, null)
        .done(function (data) {
            console.log("data", data);
            if (data.valid) {
                UmbClientMgr.closeModalWindow();
                UmbClientMgr.mainTree().syncTree(data.path, true);
            }
            else {
                alert("Action can't be done.");
            }
        }, "json");
}
