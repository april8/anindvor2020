﻿var extScope;

angular.module("umbraco").controller("A8.HtmlTableController", function ($scope, $timeout, assetsService, notificationsService, dialogService) {

    extScope = $scope;

    // default template for new renderer
    var tableTemplate = '' +
        '<table>' +
        '<tr>' +
        '<th tabindex=1>header 1</th>' +
        '<th tabindex=1>header 2</th>' +
        '</tr>' +
        '<tr>' +
        '<td tabindex=1>cell 1</td>' +
        '<td tabindex=1>cell 2</td>' +
        '</tr>' +
        '</table>';

    // attempt to get unique id for dom elements for work with jqLite (TODO: find umbraco way)
    $scope.timestamp = Math.floor(Date.now() / 1000);

    // toggle import button 
    $scope.ngToggle = false;
    $scope.doToggle = function (flag) {
        if (flag) {
            $scope.ngToggle = false;
        } else {
            $scope.ngToggle = true;
        }
    };

    // real import button action
    $scope.doImport = function (o) {
        var oTarget = angular.element("#a8TableContent-" + o);
        var oSource = angular.element("#a8TablePaste-" + o);
        if (oSource && oSource.text().length > 0) {
            // import
            if (confirm("Old table data will be replaced. Proceed?")) {
                var result = cleanTable(oSource);
                if (result.length > 0) {
                    oTarget.html(result);
                }
                else {
                    alert("It looks like this is not a table. Nothing to do.");
                    return;
                }
            }
            // clear
            oSource.html("");
            // close itself
            $scope.doToggle($scope.ngToggle);
        }
        else {
            alert("Nothing to paste.");
        }
    };

    // ZORAN: edit cell  with RTE
	$scope.editCell = function (cell, cidx) {
		
		dialogService.open({
			template: '../App_Plugins/A8HtmlTable/tinymice/views/table.editor.tinymice.html',
			show: true,
			dialogData: { text: cell.html() },
			callback: function (tinymiceValue) {

				//console.log("tinymiceValue", tinymiceValue);
				cell.html(tinymiceValue);

				dialogService.close();
			}
		});
    }

    // tell the assetsService to load the libs
    assetsService
        .load([
            "/App_Plugins/A8HtmlTable/table.js"
        ])
        .then(function () {

            setTimeout(function () {

                angular.element("#a8TableWrapper-" + $scope.timestamp).A8Table();

                // set table values
                if ($scope.model.value === null || $scope.model.value === "") {
                    $scope.model.value = tableTemplate;
                    angular.element("#a8TableContent-" + $scope.timestamp).html(tableTemplate);
                }
                else {
                    angular.element("#a8TableContent-" + $scope.timestamp).html($scope.model.value);
                }

                // get table values 
                $scope.$on("formSubmitting", function (e, args) {
                    $scope.model.value = angular.element("#a8TableContent-" + $scope.timestamp).html();
                });

            }, 0);

        });
});

// independend fn, can be jquery
function cleanTable(oSource) {
    var result = "";
    var $source = oSource;
    var $table = $source.children("table");
    if ($table && $table.length > 0) {
        $table = $table.first();
    }
    else {
        return result;
    }
    var $rows = $table.children("tbody");
    if ($rows && $rows.length > 0) {
        $rows = $rows.children("tr");
    }
    else {
        $rows = $table.children("tr");
    }
    if ($rows && $rows.length > 0) {
        result = result + "<table>";
        $rows.each(function () {
            var $columns = $(this).children("td");
            if ($columns) {
                result = result + "<tr>";
                $columns.each(function () {
                    result = result + "<td tabindex=1>" + $(this).text() + "</td>"; // detag content
                });
                result = result + "</tr>";
            }
        });
        result = result + "</table>";
    }
    return result;
}