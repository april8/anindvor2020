﻿; (function ($) {
    "use strict";
    $.fn.A8Table = function () {

        //return this.each(function () {

            // this is our parent holder
            var ident = $(this).attr("data-ident");
            var base = $("#a8TableContent-" + ident);

            // handle paste in cells
            $(document).on('paste', "#a8TableContent-" + ident, function (e) {
                window.setTimeout(function () {
                    $(e.target).html($(e.target).text());
                }, 200);
            });

            base.contextPopup({
                selector: 'th, td',
                title: 'Edit table',
                items: [
                    { command: 'editCell', label: 'Edit cell' },
                    null, // divider
                    { command: 'addheaderbefore', label: 'Add header before' },
                    { command: 'addheaderafter', label: 'Add header after' },
                    null, // divider
                    { command: 'addrowbefore', label: 'Add row before' },
                    { command: 'addrowafter', label: 'Add row after' },
                    null, // divider
                    { command: 'addcolbefore', label: 'Add column before' },
                    { command: 'addcolafter', label: 'Add column after' },
                    null, // divider
                    { command: 'removerow', label: 'Remove row' },
                    null, // divider
                    { command: 'removecol', label: 'Remove column' }
                    // leave this for reference
                    //null, // divider
                    //{ command: 'bold', label: 'Bold' },
                    //{ command: 'italic', label: 'Italic' },
                    //{ command: 'strikeThrough', label: 'Strike' },
                    //{ command: 'underline', label: 'Underline' },
                    //{ command: 'superscript', label: 'Superscript' },
                    //{ command: 'subscript', label: 'Subscript' },
                    //null, // divider
                    //{ command: 'justifyleft', label: 'Left' },
                    //{ command: 'justifycenter', label: 'Center' },
                    //{ command: 'justifyright', label: 'Right' },
                    //{ command: 'justifyfull', label: 'Justify' }
                ],
                callback: function (command, o) {
                    var cell = o;
                    var cidx = cell.index();
                    var row = cell.closest("tr");
                    var tbl = cell.closest("tr").closest("table");
                    var tagName;
                    var cells;
                    var t;
                    var r;

                    cell.attr("contenteditable", "");

                    switch (command) {
                        case "editCell":
                            {
                                // for now, action goes to angular
                                extScope.editCell(cell, cidx);
                                break;
                            }
                        case "addheaderbefore":
                        case "addrowbefore":
                            {
                                tagName = (command == "addheaderbefore" ? "th" : "td");
                                cells = row.find("td,th").length;
                                t = "<tr>";
                                for (var i = 0; i <= cells - 1; i++) {
                                    t += "<" + tagName + " tabindex=1></" + tagName + ">";
                                }
                                t += "</tr>";
                                row.before(t);
                                break;
                            }
                        case "addheaderafter":
                        case "addrowafter":
                            {
                                tagName = (command == "addheaderafter" ? "th" : "td");
                                cells = row.find("td,th").length;
                                t = "<tr>";
                                for (var i = 0; i <= cells - 1; i++) {
                                    t += "<" + tagName + " tabindex=1></" + tagName + ">";
                                }
                                t += "</tr>";
                                row.after(t);
                                break;
                            }
                        case "addcolbefore":
                            {
                                tbl.find('tr').each(function () {
                                    var z = $(this).find('td,th').eq(cidx);
                                    var zt = z.get(0).tagName;
                                    z.before("<" + zt + " tabindex=1></" + zt + ">");
                                });
                                break;
                            }
                        case "addcolafter":
                            {
                                tbl.find('tr').each(function () {
                                    var z = $(this).find('td,th').eq(cidx);
                                    var zt = z.get(0).tagName;
                                    z.after("<" + zt + " tabindex=1></" + zt + ">");
                                });
                                break;
                            }
                        case "removerow":
                            {
                                if (tbl.find("tr").length == 1) {
                                    alert("Last row cannot be removed!");
                                }
                                else {
                                    r = confirm("Confirm removing row!");
                                    if (r) {
                                        row.remove();
                                    }
                                }
                                break;
                            }
                        case "removecol":
                            {
                                if (row.find("td,th").length == 1) {
                                    alert("Last column cannot be removed!");
                                }
                                else {
                                    r = confirm("Confirm removing column!");
                                    if (r) {
                                        tbl.find('tr').each(function () {
                                            $(this).find('td,th').eq(cidx).remove();
                                        });
                                    }
                                }
                                break;
                            }
                        case "bold":
                        case "italic":
                        case "strikeThrough":
                        case "underline":
                        case "justifyleft":
                        case "justifyright":
                        case "justifycenter":
                        case "justifyfull":
                        case "superscript":
                        case "subscript":
                            {
                                document.execCommand(command, false, "");
                            }
                    }

                    cell.removeAttr("contenteditable");

                    var e = $.Event("keyup", { keyCode: 27 });
                    base.trigger(e);

                }
            });

        //});
    };
})(jQuery);