﻿angular.module('umbraco.resources').factory('a8DictionaryPickerResource',
	function ($q, $http, umbRequestHelper)
	{
		return {
			getValues: function (requestedParentDictionaryKeyword)
			{
				return umbRequestHelper.resourcePromise(
					$http.get("backoffice/A8/A8DictionaryPickerApi/GetValues?requestedParentDictionaryKeyword=" + requestedParentDictionaryKeyword)
				);
			}
		};
	}
);