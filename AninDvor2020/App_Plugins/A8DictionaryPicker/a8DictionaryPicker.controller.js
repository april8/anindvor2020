﻿angular.module("umbraco")
	.controller("A8.A8DictionaryPickerController", function ($scope, a8DictionaryPickerResource)
	{
		a8DictionaryPickerResource.getValues($scope.model.config.requestedParentDictionaryKeyword).then(function (response)
		{
			$scope.options = response;
		});
	});