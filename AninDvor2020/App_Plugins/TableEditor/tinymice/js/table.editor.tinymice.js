﻿
angular.module('umbraco.resources').factory('datatypeResourcex',
	function ($http) {
		return {
			getByName: function (name) {
				return $http.get("backoffice/A8DataType/DataType/GetByName?name=" + name);
			}
		}
	}
);

angular.module('umbraco').controller('TinyMiceTableEditorController', function ($scope, tableEditorApi, notificationsService, dialogService, datatypeResourcex) {

	$scope.model = {};
	$scope.model.value = $scope.$parent.dialogData.text;
	console.log("$scope.model.value", $scope.model.value);

	$scope.visible = false;

	datatypeResourcex.getByName('HTML - A8').then(function (result) {
		$scope.macroRte = {
			label: 'Text',
			description: '',
			view: 'rte',
			value: $scope.model.value,
			config: result.data.config
		}

		$scope.visible = true;
	});
	$scope.$watch("macroRte.value", function (newValue, oldValue) {
		if ($scope.macroRte) {
			$scope.model.value = newValue;
		}
	});
	

	$scope.close = function () {
		$scope.$parent.dialogOptions.close();
	}
});