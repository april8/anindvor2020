﻿using System.Collections.Generic;
using Umbraco.Core.Models;
using Umbraco.Web;
using Umbraco.Web.Models;

namespace A8
{
    public class PageBaseModel : RenderModel
    {
        public RenderModel Model;

        // generic
        public string DocumentTypeAlias { get; set; }

        public int CatalogueDetailId { get; set; }
        public string CatalogueDetailTypeAlias { get; set; }
        public object CatalogueDetailItem { get; set; } // mapped catalogue content

        public IPublishedContent SiteSettingsContent { get; set; }

        // seo
        public string PageHtmlTitle { get; set; }
        public string MetaKeywords { get; set; }
        public string MetaDescription { get; set; }
        public bool NoIndex { get; set; }
        public bool NoFollow { get; set; }
        public bool ExcludeFromSitemap { get; set; }

        // page settings
        public string RawHtmlHead { get; set; }
        public string RawHtmlBodyStart { get; set; }
        public string RawHtmlBodyEnd { get; set; }
        public bool ExcludeFromSearch { get; set; }

        // og tags
        public string OgImage { get; set; }
        public string OgTitle { get; set; }
        public string OgDescription { get; set; }

        public PageBaseModel(RenderModel model) : base(model.Content, model.CurrentCulture)
        {
            Model = model;

            SiteSettingsContent = A8.CommonUmbraco.GetSiteSettingsContent();

            GetGeneric();
            GetSeo();
            GetPageSettings();
            GetOgTags();
        }

        private void GetGeneric()
        {
            DocumentTypeAlias = Model.Content.DocumentTypeAlias;
            CatalogueDetailId = 0;
            CatalogueDetailTypeAlias = string.Empty;
    }

        private void GetSeo()
        {
            string seoSiteHtmlTitle = SiteSettingsContent.GetPropertyValue<string>("seoSiteHtmlTitle");
            string seoPageHtmlTitle = Model.Content.GetPropertyValue<string>("seoPageHtmlTitle");
            PageHtmlTitle = (seoSiteHtmlTitle.Length > 0 ? seoSiteHtmlTitle + " - " : string.Empty) + (seoPageHtmlTitle.Length > 0 ? seoPageHtmlTitle : Model.Content.Name);

            MetaKeywords = Model.Content.GetPropertyValue<string>("seoMetaKeywords", true);
            MetaDescription = Model.Content.GetPropertyValue<string>("seoMetaDescription", true);

            NoIndex = Model.Content.GetPropertyValue<bool>("seoNoIndex");
            NoFollow = Model.Content.GetPropertyValue<bool>("seoNoFolow");

            // todo: exclude from siemap
        }

        private void GetPageSettings()
        {
            IPublishedContent pageSettingsRawHtmls = Model.Content.GetPropertyValue<IPublishedContent>("rawHtmls");
            if (pageSettingsRawHtmls != null)
            {
                RawHtmlHead = pageSettingsRawHtmls.GetPropertyValue<string>("rawHtmlHead");
                RawHtmlBodyStart = pageSettingsRawHtmls.GetPropertyValue<string>("rawHtmlBodyStart");
                RawHtmlBodyEnd = pageSettingsRawHtmls.GetPropertyValue<string>("rawHtmlBodyEnd");
            }

            IPublishedContent siteSettingsRawHtmls = SiteSettingsContent.GetPropertyValue<IPublishedContent>("rawHtmls");
            if (siteSettingsRawHtmls != null)
            {
                RawHtmlHead += siteSettingsRawHtmls.GetPropertyValue<string>("rawHtmlHead");
                RawHtmlBodyStart += siteSettingsRawHtmls.GetPropertyValue<string>("rawHtmlBodyStart");
                RawHtmlBodyEnd += siteSettingsRawHtmls.GetPropertyValue<string>("rawHtmlBodyEnd");
            }
        }

        private void GetOgTags()
        {
            IPublishedContent ogTags = Model.Content.GetPropertyValue<IPublishedContent>("ogTags");
            if (ogTags != null)
            {
                OgImage = A8.CommonUmbraco.FormatOgImagePath(A8.Common.CheckStr(ogTags.GetPropertyValue<IPublishedContent>("ogImage")?.Url));

                OgTitle = ogTags.GetPropertyValue<string>("ogTitle");
                OgDescription = ogTags.GetPropertyValue<string>("ogDescription");

                // fallback
                if (OgTitle.Length == 0) OgTitle = PageHtmlTitle;
                if (OgDescription.Length == 0) OgDescription = MetaDescription;
            }
        }
    }
}