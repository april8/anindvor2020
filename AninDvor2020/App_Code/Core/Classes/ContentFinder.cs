﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Hosting;
using Umbraco.Core;
using Umbraco.Core.Models;
using Umbraco.Web;
using Umbraco.Web.Routing;

namespace A8.Core
{
    public class ContentFinder : IContentFinder
    {
        private IEnumerable<string> DocumentTypesToSkip404Handler
        {
            get
            {
                return new[] { "cataloguePage" };
            }
        }

        public bool TryFindContent(PublishedContentRequest contentRequest)
        {
            // ONLY FOR FRONTEND REQUESTS
            if (UmbracoContext.Current.IsFrontEndUmbracoRequest)
            {
                var umbHelper = new UmbracoHelper(UmbracoContext.Current);

                // needed paths
                string absoluteRequestedUrl = HttpUtility.UrlDecode(contentRequest.Uri.AbsoluteUri);
                string relativeRequestedUrl = HttpUtility.UrlDecode(contentRequest.Uri.AbsolutePath);
                string requestedQuery = HttpUtility.UrlDecode(contentRequest.Uri.Query);

                // Glej funkcijo spodaj!
                GetConfigurationBlockUrl(absoluteRequestedUrl);

                // skip if direct 1234.aspx query
                if (A8.Common.CheckNum(relativeRequestedUrl.Replace("/", string.Empty).Replace(".aspx", string.Empty)) > 0)
                {
                    return false;
                }

                // a route is "/path/to/page" when there is no domain, and "123/path/to/page" when there is a domain (as in our multisite case), and then 123 is the ID of the node which is the root of the domain
                // get domain name from Uri
                // find umbraco home node for uri's domain, and get the id of the node it is set on
                var umbDomainService = ApplicationContext.Current.Services.DomainService;
                var domains = umbDomainService.GetAll(true) as IList<IDomain> ?? umbDomainService.GetAll(true).ToList();
                var domainRoutePrefixId = String.Empty;
                if (domains != null && domains.Count > 0)
                {
                    // a domain is set, so we need to prefix the request to GetByRoute by the id of the node it is attached to
                    // a domain can be defined with or without http(s) so we need to check for both cases
                    IDomain domain = null;
                    foreach (IDomain currentDomain in domains)
                    {
                        if (currentDomain.DomainName.ToLower().StartsWith("http") && contentRequest.Uri.AbsoluteUri.ToLower().StartsWith(currentDomain.DomainName.ToLower()))
                        {
                            domain = currentDomain;
                            break;
                        }
                        else if ((contentRequest.Uri.Authority.ToLower() + contentRequest.Uri.AbsolutePath.ToLower()).StartsWith(currentDomain.DomainName.ToLower()))
                        {
                            domain = currentDomain;
                            break;
                        }
                    }

                    if (domain != null)
                    {
                        // the domain has a RootContentId that we can use as the prefix
                        domainRoutePrefixId = domain.RootContentId.ToString();
                    }
                }

                // MAIN STEP - if page is found -> return page, else check redirects, else return 404 
                IPublishedContent foundedUmbracoPage = TryFindPage((domainRoutePrefixId.Length == 0 ? "0" : domainRoutePrefixId) + relativeRequestedUrl);

                if (foundedUmbracoPage != null)
                {
                    contentRequest.PublishedContent = foundedUmbracoPage;

                    if (!DocumentTypesToSkip404Handler.Contains(foundedUmbracoPage.DocumentTypeAlias))
                    {
                        string foundedPageUrl = foundedUmbracoPage.Url;
                        string absoluteRequestedUrlNoQuery = absoluteRequestedUrl;
                        if (requestedQuery.Length > 0)
                        {
                            absoluteRequestedUrlNoQuery = absoluteRequestedUrl.Replace(requestedQuery, string.Empty);
                        }

                        if (!absoluteRequestedUrlNoQuery.EndsWith("/")) absoluteRequestedUrlNoQuery += "/";

                        // founded and requested pages DO NOT MATCH, so check redirect table
                        if (foundedPageUrl != absoluteRequestedUrlNoQuery)
                        {
                            // load redirect table
                            var allRedirects = A8.CommonUmbraco.Redirects();
                            string absoluteRequestedUrlNoProtocol = A8.Common.StripHttpProtocol(absoluteRequestedUrl).ToLower();
                            if (allRedirects.ContainsKey(absoluteRequestedUrlNoProtocol))
                            {
                                if (absoluteRequestedUrlNoProtocol != A8.Common.StripHttpProtocol(allRedirects[absoluteRequestedUrlNoProtocol]))
                                {
                                    // make permanent redirect
                                    HttpContext.Current.Response.RedirectPermanent(allRedirects[absoluteRequestedUrlNoProtocol], true);
                                }
                            }
                            else
                            {
                                A8.CommonUmbraco.Call404Page(foundedUmbracoPage);
                            }
                        }
                        else
                        {
                            // founded and requested pages DO MATCH, so check browser lang

                            // language root document must have alias languageFirstPage defined via umbraco admin
                            if (foundedUmbracoPage.DocumentTypeAlias == "site")
                            {
                                // default language if anything failed
                                string redirect = "sl";

                                var siteLangs = foundedUmbracoPage.Site().Children;
                                string[] browserLangs = null;
                                string serverVariables = A8.Common.CheckStr(HttpContext.Current.Request.ServerVariables["HTTP_ACCEPT_LANGUAGE"]);
                                if (serverVariables.Length > 0)
                                {
                                    browserLangs = A8.Common.CheckStr(HttpContext.Current.Request.ServerVariables["HTTP_ACCEPT_LANGUAGE"]).Split(',');
                                }
                                if (browserLangs != null && browserLangs.Length > 0)
                                {
                                    if (siteLangs != null && siteLangs.Any())
                                    {
                                        string browserLang = string.Empty;
                                        string siteLang = string.Empty;
                                        string tmpSiteLang = string.Empty;
                                        bool langDetected = false;
                                        foreach (string bLang in browserLangs)
                                        {
                                            if (!langDetected)
                                            {
                                                browserLang = A8.Common.CheckStr(A8.Common.CheckStr(bLang.Split(new char[] { ';' })[0]).Split(new char[] { '-' })[0]).ToLower();
                                                foreach (var sLang in siteLangs)
                                                {
                                                    if (!langDetected)
                                                    {
                                                        tmpSiteLang = A8.Common.CheckStr(sLang.GetCulture());
                                                        siteLang = A8.Common.CheckStr(A8.Common.CheckStr(tmpSiteLang.Split(new char[] { ';' })[0]).Split(new char[] { '-' })[0]).ToLower();

                                                        if (browserLang == siteLang)
                                                        {
                                                            langDetected = true;
                                                            redirect = siteLang;
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                                HttpContext.Current.Response.Redirect("/" + redirect + "/");
                            }
                        }
                    }
                    return true;
                }
                else
                {
                    // old
                    // nothing to do here while 'if' allways returns something (at least there is root node)
                    // old
                }
            }
            return false;
        }

        // the loop: [] = iteration
        // 123[5]/aaa[4]/bbb[3]/ccc[2]/ddd[1]
        private IPublishedContent TryFindPage(string path)
        {
            if (!path.EndsWith("/"))
            {
                path += "/";
            }
            IPublishedContent requestedPage = UmbracoContext.Current.ContentCache.GetByRoute(path);
            if (requestedPage == null)
            {
                if (path.EndsWith("/"))
                {
                    path = path.Remove(path.Length - 1);
                }
                path = String.Join("/", path.Split(new char[] { '/' }, StringSplitOptions.RemoveEmptyEntries).SkipLast());
                requestedPage = TryFindPage(path);
            }
            // return page or null
            return requestedPage;
        }

        // V primeru, da se en jezik objavi pred ostalimi, ostali se pa še urejajo, je potrebno nekako blokirati te jezike
        // za splošne obiskovalce in omogočiti predogled za urednike. Prvi poskus je bil z redirect-ruls-i a na azure niso delovali.
        // Zato se sedaj uporabi ta funkcija. V web.configu se določi redirekte-pare: zahtevane strani -> ciljne strani.
        // Ta funkcija potem prebere te pare in ustrezno redirecta. To se uporablja samo za čas urejanja. Ko je določen jezik urejen, se iz
        // web.configa odstrani ta par. Seveda je za potrebe urejanja potrebno ustvariti 'servisno' domeno, ponavadi se naredi kar na freenom-u npr. rogaskaslatina209.tk.
        // Ker te domene ni naštete tu, se noramlno odpre en/, de/, ru.
        //<BlockUrl>
        //  <UrlPairs>
        //    <add key = "visit-rogaska-slatina.si/en" value="https://www.visit-rogaska-slatina.si/si/" />
        //    <add key = "visit-rogaska-slatina.si/de" value="https://www.visit-rogaska-slatina.si/si/" />
        //    <add key = "visit-rogaska-slatina.si/ru" value="https://www.visit-rogaska-slatina.si/si/" />
        //  </UrlPairs>
        //</BlockUrl>
        public static void GetConfigurationBlockUrl(string currentRequest)
        {
            // samo za frontend requeste
            if (UmbracoContext.Current.IsFrontEndUmbracoRequest)
            {
                var UrlPairs = ConfigurationManager.GetSection("BlockUrl/UrlPairs") as NameValueCollection;
                if (UrlPairs != null && UrlPairs.Count > 0)
                {
                    foreach (var key in UrlPairs.AllKeys)
                    {
                        if (currentRequest.Contains(key))
                        {
                            HttpContext.Current.Response.Redirect(UrlPairs[key]);
                        }
                    }
                }
            }
        }
    }
}