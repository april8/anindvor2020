﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
//using static A8.NuPickersEnum;


namespace A8
{
    public static class ImageVariations
    {
        [Obsolete]
        public static string GeneratePictureTag(string url, string variation, string alt = "", string title = "", string cls = "", int maxWidth = 0)
        {
            string pictureTag = string.Empty;
            if (url.Length > 0)
            {
                pictureTag += "<picture>\n";
                pictureTag += "	<source media=\"(max-width: 600px)\" srcset=\"" + HttpUtility.UrlPathEncode("/imagelib/" + (maxWidth > 0 && maxWidth < 960 ? maxWidth : 960) + "-" + variation + url) + "\" >\n"; // 960
                pictureTag += "	<source media=\"(max-width: 900px)\" srcset=\"" + HttpUtility.UrlPathEncode("/imagelib/" + (maxWidth > 0 && maxWidth < 1360 ? maxWidth : 1360) + "-" + variation + url) + "\" >\n"; // 1360
                pictureTag += "	<source media=\"(max-width: 1200px)\" srcset =\""+ HttpUtility.UrlPathEncode("/imagelib/" + (maxWidth > 0 && maxWidth < 1840 ? maxWidth : 1840) + "-" + variation + url) + "\" >\n"; // 1840
                pictureTag += "	<source media=\"(max-width: 1800px)\" srcset =\"" + HttpUtility.UrlPathEncode("/imagelib/" + (maxWidth > 0 && maxWidth < 1920 ? maxWidth : 1920) + "-" + variation + url) + "\" >\n"; // 1920
                pictureTag += "	<source media=\"(min-width: 1801px)\" srcset =\"" + HttpUtility.UrlPathEncode("/imagelib/" + (maxWidth > 0 && maxWidth < 2560 ? maxWidth : 2560) + "-" + variation + url) + "\" >\n"; // 2560
                pictureTag += "	<img data-src=\"" + HttpUtility.UrlPathEncode("/imagelib/" + (maxWidth > 0 && maxWidth < 640 ? maxWidth : 640) + "-" + variation + url) + "\" alt =\"" + HttpUtility.HtmlEncode(alt) + "\" title=\"" + HttpUtility.HtmlEncode(title) + "\" " + (cls.Length > 0 ? "class=\"" + cls + "\"" : string.Empty) + ">\n";
                pictureTag += "</picture>\n";
            }

            return pictureTag;
        }
    }
}