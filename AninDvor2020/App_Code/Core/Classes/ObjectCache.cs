﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace A8
{
    public static class CacheExtensions
    {
        // Usage: HttpRuntime.Cache.GetCachedItem("myKey", () => GetSomethingToCache());
        // Usage: HttpRuntime.Cache.RemoveCachedItem("myKey");
        // Usage: HttpRuntime.Cache.RemoveCache();

        private static readonly object sync = new object();

        public static void RemoveCachedItem(this System.Web.Caching.Cache cache, string key)
        {
            cache.Remove(key);
        }

        public static void RemoveCache(this System.Web.Caching.Cache cache)
        {
            cache.RemoveCache();
        }

        public static T GetCachedItem<T>(this System.Web.Caching.Cache cache, string key, Func<T> generator)
        {
            return cache.GetCachedItem(key, generator, System.Web.Caching.Cache.NoAbsoluteExpiration, System.Web.Caching.Cache.NoSlidingExpiration);
        }

        public static T GetCachedItem<T>(this System.Web.Caching.Cache cache, string key, Func<T> generator, DateTime absoluteExpiration)
        {
            return cache.GetCachedItem(key, generator, absoluteExpiration, System.Web.Caching.Cache.NoSlidingExpiration);
        }

        public static T GetCachedItem<T>(this System.Web.Caching.Cache cache, string key, Func<T> generator, DateTime absoluteExpiration, TimeSpan slidingExpiration)
        {
            var result = cache[key];
            if (result == null)
            {
                lock (sync)
                {
                    result = cache[key];
                    if (result == null)
                    {
                        result = generator();
                        cache.Insert(key, result, null, absoluteExpiration, slidingExpiration);
                    }
                }
            }
            return (T)result;
        }
    }
}