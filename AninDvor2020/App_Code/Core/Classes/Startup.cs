﻿using Owin;
using Umbraco.Web;
using Hangfire;
using System;

namespace A8
{
    public class Startup : UmbracoDefaultOwinStartup
    {
        public override void Configuration(IAppBuilder app)
        {
            base.Configuration(app);

            // Configure the database where Hangfire is going to create its tables
            var cs = Umbraco.Core.ApplicationContext.Current.DatabaseContext.ConnectionString;
            GlobalConfiguration.Configuration.UseSqlServerStorage(cs);

            var dashboardOptions = new DashboardOptions
            {
                Authorization = new[]
                {
                    new UmbracoUserAuthorizationFilter()
                }
            };

            // do custom actions at startup

            // hangfire stuff
            A8.HangfireJobs.InitJobs(app);
            // Configure Hangfire's dashboard with auth configuration
            app.UseHangfireDashboard("/hangfire", dashboardOptions);
            // Create a default Hangfire server
            app.UseHangfireServer();
        }

    }
}