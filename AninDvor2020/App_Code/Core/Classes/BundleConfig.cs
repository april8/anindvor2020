﻿using AutoprefixerHost;
using BundleTransformer.Core.Builders;
using BundleTransformer.Core.Bundles;
using BundleTransformer.Core.Orderers;
using BundleTransformer.Core.Resolvers;
using BundleTransformer.Core.Transformers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Optimization;

namespace A8.Core
{
    public class BundleConfig
    {
        public static void RegisterBundles(BundleCollection bundles)
        {
            BundleTable.EnableOptimizations = true;

            var nullBuilder = new NullBuilder();
            var nullOrderer = new NullOrderer();

            BundleResolver.Current = new CustomBundleResolver();

            // head css
            var css = new CustomStyleBundle("~/bundles/styles/head.css")
                .Include("~/styles/main/core/normalize.less")
                .Include("~/styles/main/core/variables.less")
                .Include("~/styles/main/theme/global.less")
                .Include("~/styles/main/theme/structure.less")
                .Include("~/styles/main/theme/garden4.less")
                .Include("~/styles/main/theme/revolution-slider-settings.less")
                .IncludeDirectory("~/styles/head", "*.less", true)
            ;

            css.Builder = nullBuilder;
            css.Orderer = nullOrderer;

            css.Transforms.Add(new LessTransform());
            css.Transforms.Add(new AutoprefixTransform());
            css.Transforms.Add(new CssMinify());
            bundles.Add(css);

            // main css
            css = new CustomStyleBundle("~/bundles/styles/main.css")
                .Include("~/styles/main/core/variables.less")
                .Include("~/styles/main/core/normalize.less")
                .Include("~/styles/main/core/core.less")
                .Include("~/styles/main/core/tags.less")
                .Include("~/styles/main/core/tags-margins.less")
                .Include("~/styles/main/core/helpers.less")
                .IncludeDirectory("~/styles/main/plugins/", "*.less", true)
                .IncludeDirectory("~/styles/main/renderers/", "*.less", true)
                .IncludeDirectory("~/styles/main/components/", "*.less", true)
            ;

            css.Builder = nullBuilder;
            css.Orderer = nullOrderer;

            css.Transforms.Add(new LessTransform());
            css.Transforms.Add(new AutoprefixTransform());
            css.Transforms.Add(new CssMinify());
            bundles.Add(css);

            var js = new CustomScriptBundle("~/bundles/scripts/main.js")
                .Include("~/Scripts/jquery-ui.js")
                .Include("~/Scripts/jquery.magnific-popup.js")
                .Include("~/Scripts/lazyload.min.js")
                .Include("~/Scripts/slick.min.js")
                .Include("~/Scripts/cookieconsent.min.js")
                .Include("~/Scripts/common.js");

            js.Builder = nullBuilder;
            js.Orderer = nullOrderer;

            js.Transforms.Add(new JsMinify());
            bundles.Add(js);
        }
    }

    public class LessTransform : IBundleTransform
    {
        public void Process(BundleContext context, BundleResponse response)
        {
            response.Content = dotless.Core.Less.Parse(response.Content);
            response.ContentType = "text/css";
        }
    }

    public class AutoprefixTransform : IBundleTransform
    {
        public void Process(BundleContext context, BundleResponse response)
        {
            var options = new ProcessingOptions
            {
                Cascade = true,
                Add = true,
                Remove = true,
                Supports = false,
                Flexbox = FlexboxMode.All,
                Grid = GridMode.None,
                IgnoreUnknownVersions = false,
                Stats = "",
                Browsers = new List<string> { "> 2%", "last 2 version" }
            };

            var autoprefixer = new Autoprefixer(options);
            ProcessingResult result = autoprefixer.Process(response.Content);
            response.Content = result.ProcessedContent;
            response.ContentType = "text/css";
        }
    }
}