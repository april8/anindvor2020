﻿using System.Web;
using Umbraco.Core;
using Umbraco.Core.Events;
using Umbraco.Core.Models;
using Umbraco.Core.Publishing;
using Umbraco.Core.Services;
using Umbraco.Web.Editors;
using Umbraco.Web.Models.ContentEditing;

namespace A8
{
    public class ContentEditorEventHandler : ApplicationEventHandler
    {
        public ContentEditorEventHandler()
        {
            EditorModelEventManager.SendingContentModel += EditorModelEventManager_SendingContentModel;

            ContentService.Saving += this.ContentServiceSaving;
            ContentService.Publishing += this.ContentServicePublishing;


        }

        // disable publish
        // [this code is for reference here - use it if you need it]
        private void ContentServicePublishing(IPublishingStrategy sender, PublishEventArgs<Umbraco.Core.Models.IContent> e)
        {
            //bool isEventEditor = false;
            //bool isEventPublisher = false;
            //bool isAlsoSomethingElse = false;
            //bool isCatEvent = false;

            //// get user's group
            //var userService = ApplicationContext.Current.Services.UserService;
            //var currentUser = userService.GetByUsername(HttpContext.Current.User.Identity.Name);
            //foreach (var group in currentUser.Groups)
            //{
            //    if (group.Alias.ToLower() == "eventseditors")
            //    {
            //        isEventEditor = true;
            //    }
            //    if (group.Alias.ToLower() == "eventspublishers")
            //    {
            //        isEventPublisher = true;
            //    }
            //    if (group.Alias.ToLower() != "eventseditors" && group.Alias.ToLower() != "eventspublishers")
            //    {
            //        isAlsoSomethingElse = true;
            //    }
            //}

            //foreach (var item in e.PublishedEntities)
            //{
            //    if (item.ContentType.Alias.ToLower() == "catevent")
            //    {
            //        isCatEvent = true;
            //        break;
            //    }
            //}

            //if ((isEventPublisher || isEventEditor) && !isCatEvent && !isAlsoSomethingElse)
            //{
            //    e.Messages.Add(new EventMessage("Restricted publish", "User can't publish the document", EventMessageType.Warning));
            //    e.Cancel = true;
            //}
        }

        // disable save
        // [this code is for reference here - use it if you need it]
        private void ContentServiceSaving(IContentService sender, SaveEventArgs<IContent> e)
        {
            //bool isEventEditor = false;
            //bool isEventPublisher = false;
            //bool isAlsoSomethingElse = false;
            //bool isCatEvent = false;

            //// get user's group
            //var userService = ApplicationContext.Current.Services.UserService;
            //var currentUser = userService.GetByUsername(HttpContext.Current.User.Identity.Name);
            //if (currentUser != null) // skip api calls
            //{
            //    foreach (var group in currentUser.Groups)
            //    {
            //        if (group.Alias.ToLower() == "eventseditors")
            //        {
            //            isEventEditor = true;
            //        }
            //        if (group.Alias.ToLower() == "eventspublishers")
            //        {
            //            isEventPublisher = true;
            //        }
            //        if (group.Alias.ToLower() != "eventseditors" && group.Alias.ToLower() != "eventspublishers")
            //        {
            //            isAlsoSomethingElse = true;
            //        }
            //    }

            //    foreach (var item in e.SavedEntities)
            //    {
            //        if (item.ContentType.Alias.ToLower() == "catevent")
            //        {
            //            isCatEvent = true;
            //            break;
            //        }
            //    }

            //    // disable save completely
            //    if (((isEventEditor && !isCatEvent) || isEventPublisher) && !isAlsoSomethingElse)
            //    {
            //        e.Messages.Add(new EventMessage("Restricted save", "User can't save the document", EventMessageType.Warning));
            //        e.Cancel = true;
            //    }
            //}
        }

        // disable preview
        private void EditorModelEventManager_SendingContentModel(System.Web.Http.Filters.HttpActionExecutedContext sender, EditorModelEventArgs<Umbraco.Web.Models.ContentEditing.ContentItemDisplay> e)
        {
            string documentTypeAlias = e.Model.ContentTypeAlias;

            // generally disable for forms and catalogues
            if (documentTypeAlias.StartsWith("cat_") || documentTypeAlias.StartsWith("frm_"))
            {
                e.Model.AllowPreview = false;
            }

            // disable for custom doc types
            switch (documentTypeAlias)
            {
                case "siteSettings":
                    {
                        e.Model.AllowPreview = false;
                        break;
                    }
            }

            //// [this code is for reference here - use it if you need it]
            //bool isEventEditor = false;
            //bool isEventPublisher = false;
            //bool isAlsoSomethingElse = false;
            //bool isCatEvent = false;

            //// get user's group
            //var userService = ApplicationContext.Current.Services.UserService;
            //var currentUser = userService.GetByUsername(HttpContext.Current.User.Identity.Name);

            //if (currentUser != null) // skip api calls
            //{
            //    foreach (var group in currentUser.Groups)
            //    {
            //        if (group.Alias.ToLower() == "eventseditors")
            //        {
            //            isEventEditor = true;
            //        }
            //        if (group.Alias.ToLower() == "eventspublishers")
            //        {
            //            isEventPublisher = true;
            //        }
            //        if (group.Alias.ToLower() != "eventseditors" && group.Alias.ToLower() != "eventspublishers")
            //        {
            //            isAlsoSomethingElse = true;
            //        }

            //    }

            //    // disable preview button
            //    if ((isEventEditor || isEventPublisher) && !isAlsoSomethingElse)
            //    {
            //        var contentModel = e.Model as ContentItemDisplay;
            //        if (contentModel != null)
            //        {
            //            contentModel.AllowPreview = false;
            //        }
            //    }

            //    // disable all buttons for eventpublisher
            //    if (isEventPublisher && !isAlsoSomethingElse)
            //    {
            //        var contentModel = e.Model as ContentItemDisplay;
            //        if (contentModel != null)
            //        {
            //            contentModel.AllowedActions = null;
            //        }
            //    }
            //}
        }
    }
}