﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Umbraco.Core;
using Umbraco.Core.Events;
using Umbraco.Core.Models;
using Umbraco.Core.Publishing;
using Umbraco.Core.Services;
using Umbraco.Core.Models.Membership;

namespace A8.Core
{
    public class PreventDeletionEventHandler : ApplicationEventHandler
    {

        public PreventDeletionEventHandler()
        {
            ContentService.Trashing += this.ContentServiceTrashing;
            ContentService.UnPublishing += this.ContentServiceUnPublishing;
        }

        private IEnumerable<string> ContentTypes
        {
            get
            {
                return new[]
                {
                    "site"
                    ,"cat_Catalogues"
                    ,"frm_Forms"
                    ,"firstPage"
                    ,"notFound404"
                    ,"siteIndex"
                    ,"siteMap"
                    ,"siteSettings"
                    ,"robotsTxt"
                };
            }
        }

        private void ContentServiceTrashing(IContentService sender, MoveEventArgs<IContent> e)
        {
            bool isAdministrator = false;

            // check admin (doc type)
            var userService = ApplicationContext.Current.Services.UserService;
            var currentUser = userService.GetByUsername(HttpContext.Current.User.Identity.Name);
            foreach (var group in currentUser.Groups)
            {
                if (group.Alias.ToLower() == "admin")
                {
                    isAdministrator = true;
                }
            }

            // allow only to admin
            if (!isAdministrator)
            {
                var aliasName = e.MoveInfoCollection.FirstOrDefault().Entity.ContentType.Alias;

                if (ContentTypes.Contains(aliasName))
                {
                    e.Cancel = true;
                    e.Messages.Add(CreateWarningMessage(aliasName));
                }
            }
        }

        private void ContentServiceUnPublishing(IPublishingStrategy sender, PublishEventArgs<IContent> e)
        {
            bool isAdministrator = false;

            // check admin (doc type)
            var userService = ApplicationContext.Current.Services.UserService;
            IUser currentUser = null;
            if (HttpContext.Current != null)
            {
                currentUser = userService.GetByUsername(HttpContext.Current.User.Identity.Name);
            }
            if (currentUser != null)
            {
                foreach (var group in currentUser.Groups)
                {
                    if (group.Alias.ToLower() == "admin")
                    {
                        isAdministrator = true;
                    }
                }
            }

            // allow only to admin
            if (!isAdministrator)
            {
                foreach (var item in e.PublishedEntities)
                {
                    if (ContentTypes.Contains(item.ContentType.Alias))
                    {
                        e.Cancel = true;
                        e.Messages.Add(CreateWarningMessage(item.ContentType.Alias));
                        break;
                    }
                }
            }
        }

        private static EventMessage CreateWarningMessage(string aliasName)
        {
            var eventMessage = new EventMessage("Restricted", string.Format("Current user cannot unpublish or delete {0}.", aliasName), EventMessageType.Warning);
            return eventMessage;
        }

    }
}