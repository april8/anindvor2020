﻿using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Drawing;
using System.IO;
using System.Drawing.Text;
using System.Drawing.Imaging;
using System.Runtime.InteropServices;
using System.Drawing.Drawing2D;
using System.Collections.Generic;
using System.Collections;
using System.Net;
using System.Web.Hosting;
using Umbraco.Core;
using System.Xml;
using Umbraco.Core.Models;
using Umbraco.Core.Models.EntityBase;
using Newtonsoft.Json.Linq;
using ImageProcessor;
using ImageProcessor.Imaging;

namespace A8
{
    public class Imagelib
    {
        private object supportedTypes;

        private Dictionary<string, A8.ImagelibVariation> Variations { get; set; }

        private void Initialize()
        {
            Variations = ImagelibVariations();
        }

        public Imagelib()
        {
            Initialize();
        }

        public bool ProcessQuery(string query, ref string error)
        {
            string[] path;
            bool retVal = false;

            path = query.Split('/');
            if (path.GetLength(0) > 1)
            {
                string folder = path[0];
                if (Variations.ContainsKey(folder))
                {
                    A8.ImagelibVariation variation = Variations[folder];

                    string relativeSource = string.Empty;
                    for (int x = 1; x <= path.GetLength(0) - 1; x++) relativeSource += ((relativeSource != "") ? "/" : "") + path[x];

                    string sourcePath = A8.Common.FixFolderName(HostingEnvironment.MapPath("~/" + relativeSource));
                    string destinationPath = A8.Common.FixFolderName(HostingEnvironment.MapPath("~/imagelib/" + query));

                    if (System.IO.File.Exists(sourcePath))
                    {
                        // first try to load image
                        using (ImageFactory imageFactory = new ImageFactory(false, false))
                        {
                            bool resized = false;
                            ResizeLayer layer = null;

                            // process according to Variations
                            switch (variation.Method.ToLower())
                            {
                                case "fit":
                                    {
                                        layer = new ResizeLayer(new Size(variation.Width, variation.Height), ResizeMode.Max)
                                        {
                                            Upscale = false
                                        };
                                        resized = true;
                                        break;
                                    }
                                case "crop":
                                    {
                                        layer = new ResizeLayer(new Size(variation.Width, variation.Height), ResizeMode.Crop)
                                        {
                                            Upscale = false
                                        };
                                        resized = true;
                                        break;
                                    }
                                case "orig":
                                    {
                                        layer = new ResizeLayer(new Size(variation.Width, 0), ResizeMode.Min)
                                        {
                                            Upscale = false
                                        };
                                        resized = true;
                                        break;
                                    }
                                case "raw":
                                    {
                                        layer = new ResizeLayer(new Size(2560, 0), ResizeMode.Max)
                                        {
                                            Upscale = false
                                        };
                                        resized = true;
                                        break;
                                    }
                                default:
                                    {
                                        break;
                                    }
                            }

                            if (resized && layer != null)
                            {
                                // get/set folder
                                try
                                {
                                    FileInfo fi = new FileInfo(destinationPath);
                                    Directory.CreateDirectory(fi.DirectoryName);

                                    // resize
                                    imageFactory.Quality(100).Load(sourcePath).Resize(layer);

                                    // save
                                    imageFactory.Save(destinationPath);
                                    retVal = true;
                                }
                                catch (Exception ex)
                                {
                                    error = ex.Message;
                                }
                            }

                            //// old implementation in rather problematic while
                            //// converts png to 32-bith depth with alpha-channel, thus enlarge file by 10x
                            ///
                            //if (image != null)
                            //{
                            //    FileInfo fi = new FileInfo(destinationPath);
                            //    Directory.CreateDirectory(fi.DirectoryName);
                            //    bool resized = false;

                            //    // process according to Variations
                            //    switch (variation.Method.ToLower())
                            //    {
                            //        case "fit":
                            //            {
                            //                ResizeLayer layer = new ResizeLayer(new Size(variation.Width, variation.Height), ResizeMode.Max)
                            //                {
                            //                    Upscale = false
                            //                };
                            //                image = imageFactory.Resize(layer).Image;
                            //                resized = true;
                            //                break;
                            //            }
                            //        case "crop":
                            //            {
                            //                ResizeLayer layer = new ResizeLayer(new Size(variation.Width, variation.Height), ResizeMode.Crop);
                            //                image = imageFactory.Resize(layer).Image;
                            //                resized = true;
                            //                break;
                            //            }
                            //        case "orig":
                            //            {
                            //                ResizeLayer layer = new ResizeLayer(new Size(variation.Width, 0), ResizeMode.Min);
                            //                image = imageFactory.Resize(layer).Image;
                            //                resized = true;
                            //                break;
                            //            }
                            //        case "raw":
                            //            {
                            //                if (image.Width > 2560)
                            //                {
                            //                    ResizeLayer layer = new ResizeLayer(new Size(2560, 0), ResizeMode.Max);
                            //                    image = imageFactory.Resize(layer).Image;
                            //                }
                            //                resized = true;
                            //                break;
                            //            }
                            //        default:
                            //            {
                            //                break;
                            //            }
                            //    }
                            //    if (resized)
                            //    {
                            //        image.Save(destinationPath);
                            //        retVal = true;
                            //    }
                            //}
                        }
                    }
                }
            }
            return retVal;
        }

        private Dictionary<string, ImagelibVariation> ImagelibVariations()
        {
            return (Dictionary<string, ImagelibVariation>)ApplicationContext.Current.ApplicationCache.RuntimeCache.GetCacheItem("imagelibvariations", () =>
            {
                var dict = new Dictionary<string, ImagelibVariation>();
                string variationsXml = System.IO.File.ReadAllText(HostingEnvironment.MapPath("~/imagelib/variations.xml"));
                XmlDocument xmlDoc = new XmlDocument();
                xmlDoc.LoadXml(variationsXml);

                XmlNodeList variations = xmlDoc.DocumentElement.SelectNodes("/variations/variation");
                if (variations != null && variations.Count > 0)
                {
                    foreach (XmlNode variation in variations)
                    {
                        string _folder = string.Empty;
                        string _method = "crop";
                        int _width = 2560;
                        int _height = 2560;
                        int _quality = 95;

                        XmlAttributeCollection attrs = variation.Attributes;
                        foreach (XmlAttribute attr in attrs)
                        {
                            switch (attr.Name)
                            {
                                case "Folder":
                                    {
                                        _folder = A8.Common.CheckStr(variation.Attributes["Folder"].Value);
                                        break;
                                    }
                                case "Method":
                                    {
                                        _method = A8.Common.CheckStr(variation.Attributes["Method"].Value);
                                        break;
                                    }
                                case "Width":
                                    {
                                        _width = A8.Common.CheckNum(variation.Attributes["Width"].Value);
                                        break;
                                    }
                                case "Height":
                                    {
                                        _height = A8.Common.CheckNum(variation.Attributes["Height"].Value);
                                        break;
                                    }
                                case "Quality":
                                    {
                                        _quality = A8.Common.CheckNum(variation.Attributes["Quality"].Value);
                                        break;
                                    }
                            }
                        }

                        if (_folder.Length > 0 && !dict.ContainsKey(_folder))
                        {
                            ImagelibVariation iv = new ImagelibVariation();
                            iv.Folder = _folder;
                            iv.Method = _method;
                            iv.Width = _width;
                            iv.Height = _height;
                            iv.Quality = _quality;

                            dict.Add(_folder, iv);
                        }
                    }
                }
                return dict;
            });
        }

        public bool DeleteImageVariations(IMedia item)
        {
            if (item != null)
            {
                // parse out just folder and delete folder with 'old' image(s) + umb variants
                string mediaUrl = A8.Common.CheckStr(item.GetValue<string>("umbracoFile"));
                var oUrl = JObject.Parse(mediaUrl);
                string url = Convert.ToString(oUrl["src"]); // /media/1009/creativboard-converted-01.jpg
                if (url.Length > 0)
                {
                    string[] aUrl = url.Split(new char[] { '/' });
                    if (aUrl.Length == 4)
                    {
                        if (Variations != null && Variations.Count > 0)
                        {
                            foreach (KeyValuePair<string, ImagelibVariation> variation in Variations)
                            {
                                string folder = A8.Common.FixFolderName(HostingEnvironment.MapPath("~/imagelib/" + variation.Key + "/" + aUrl[0] + "/" + aUrl[1]));
                                try
                                {
                                    if (Directory.Exists(folder))
                                    {
                                        Directory.Delete(folder, true);
                                    }
                                }
                                catch { };
                            }
                        }
                    }
                }
            }
            return true;
        }
    }

    public class ImagelibVariation
    {
        public ImagelibVariation()
        { }

        public string Folder { get; set; }
        public string Method { get; set; }
        public int Width { get; set; }
        public int Height { get; set; }
        public int Quality { get; set; }
    }
}