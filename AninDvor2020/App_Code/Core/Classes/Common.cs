﻿using Newtonsoft.Json;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Drawing;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Reflection;
using System.Security.Cryptography;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Web;
using System.Web.Hosting;
using System.Web.Mvc;
using System.Xml;
using Umbraco.Core;
using Umbraco.Core.Models;
using Umbraco.Web;

namespace A8
{
    public static class Common
    {
        static Random randomObject;

        /// <summary>
        /// Validates and returns string value as integer with default value if error occurs.
        /// </summary>
        /// <param name="num"></param>
        /// <returns></returns>
        public static Int32 CheckNum(string num)
        {
            return CheckNum(num, 0);
        }
        public static Int32 CheckNum(string num, int def)
        {
            Int32 result = def;
            if (Int32.TryParse(num, out result))
            {
                return result;
            }
            else
                return def;
        }
        public static Int32 CheckNum(object num)
        {
            return CheckNum(num, 0);
        }
        public static Int32 CheckNum(object num, int def)
        {
            if ((num == DBNull.Value) || (num == null))
                return def;
            else
                return CheckNum(num.ToString(), def);
        }
        public static int? CheckNum(string num, int? def)
        {
            Int32 result = 0;
            if (Int32.TryParse(num, out result))
            {
                return result;
            }
            else
                return def;
        }
        public static int? CheckNum(object num, int? def)
        {
            if ((num == DBNull.Value) || (num == null))
                return def;
            else
                return CheckNum(num.ToString(), def);
        }

        public static Int64 CheckNumLong(string num)
        {
            return CheckNumLong(num, 0);
        }
        public static Int64 CheckNumLong(string num, int def)
        {
            Int64 result = def;
            if (Int64.TryParse(num, out result))
            {
                return result;
            }
            else
                return def;
        }
        public static Int64 CheckNumLong(object num)
        {
            return CheckNumLong(num, 0);
        }
        public static Int64 CheckNumLong(object num, int def)
        {
            if ((num == DBNull.Value) || (num == null))
                return def;
            else
                return CheckNumLong(num.ToString(), def);
        }

        /// <summary>
        /// Validates and returns string value as decimal with default value if error occurs.
        /// </summary>
        /// <param name="num"></param>
        /// <returns></returns>
        public static Decimal CheckNumDecimal(string num)
        {
            return CheckNumDecimal(num, 0);
        }
        public static Decimal CheckNumDecimal(string num, Decimal def)
        {
            return CheckNumDecimal(num, def, ",", ".");
        }
        public static Decimal CheckNumDecimal(string num, Decimal def, string decimalSeparator, string groupSeparator)
        {
            if (num == null) return def;

            int groupPosition = num.IndexOf(groupSeparator);
            int decimalPosition = num.IndexOf(decimalSeparator);

            // check if there are both "." and "," in the string. If so, first "." is removed and "," is replaced with "." 1.250,20 -> 1250.20
            if ((groupPosition >= 0) && (decimalPosition >= 0) && (decimalPosition > groupPosition))
            {
                num = num.Replace(groupSeparator.ToString(), "");
                num = num.Replace(decimalSeparator.ToString(), ".");
            }
            else
            {
                if ((groupPosition == -1) && (decimalPosition >= 0)) // 120,20 -> 120.20
                {
                    num = num.Replace(decimalSeparator.ToString(), ".");
                }
            }

            Decimal result = def;
            NumberStyles style = NumberStyles.Number;
            CultureInfo culture = CultureInfo.CreateSpecificCulture("en-GB");
            if (Decimal.TryParse(num, style, culture, out result))
            {
                return result;
            }
            else
                return def;
        }
        public static Decimal CheckNumDecimal(object num)
        {
            return CheckNumDecimal(num, 0);
        }
        public static Decimal CheckNumDecimal(object num, Decimal def)
        {
            if ((num == DBNull.Value) || (num == null))
                return def;
            else
                return CheckNumDecimal(num.ToString(), def);
        }

        /// <summary>
        /// Validates and returns string value as double with default value if error occurs.
        /// </summary>
        /// <param name="dbl"></param>
        /// <returns></returns>
        public static Double CheckNumDouble(string dbl)
        {
            return CheckNumDouble(dbl, 0);
        }
        public static Double CheckNumDouble(string dbl, double def)
        {
            Double result = def;
            if (Double.TryParse(dbl, out result))
            {
                return result;
            }
            else
                return def;
        }
        public static Double CheckNumDouble(object dbl)
        {
            return CheckNumDouble(dbl, 0);
        }
        public static Double CheckNumDouble(object dbl, double def)
        {
            if ((dbl == DBNull.Value) || (dbl == null))
                return def;
            else
                return CheckNumDouble(dbl.ToString(), def);
        }

        /// <summary>
        /// Checks if object has valid string value, if not (is NULL) returns empty string.
        /// </summary>
        /// <param name="str"></param>
        /// <returns></returns>
        public static string CheckStr(object str)
        {
            return CheckStr(str, "");
        }

        /// <summary>
        /// Checks if object has valid string value, if not (is NULL) returns empty string.
        /// </summary>
        /// <param name="str"></param>
        /// <returns></returns>
        public static string CheckStr(string str)
        {
            return CheckStr(str, "");
        }

        /// <summary>
        /// Checks if object has valid string value, if not (is NULL) returns default string.
        /// </summary>
        /// <param name="str"></param>
        /// <param name="def"></param>
        /// <returns></returns>
        public static string CheckStr(string str, string def)
        {
            if ((str == null))
                return def;
            else
                return str.ToString();
        }

        /// <summary>
        /// Checks if object has valid string value, if not (is NULL or empty) returns default string.
        /// </summary>
        /// <param name="str"></param>
        /// <param name="def"></param>
        /// <returns></returns>
        public static string CheckStr(string str, string def, bool defIfEmpty)
        {
            if ((str == null) || (defIfEmpty && str == ""))
                return def;
            else
                return str.ToString();
        }

        /// <summary>
        /// Checks if object has valid string value, if not (is NULL) returns default string.
        /// </summary>
        /// <param name="str"></param>
        /// <param name="def"></param>
        /// <returns></returns>
        public static string CheckStr(object str, string def)
        {
            if ((str == DBNull.Value) || (str == null))
                return def;
            else
                return str.ToString();
        }

        /// <summary>
        /// Checks if object has valid string value, if not (is NULL or empty) returns default string.
        /// </summary>
        /// <param name="str"></param>
        /// <param name="def"></param>
        /// <returns></returns>
        public static string CheckStr(object str, string def, bool defIfEmpty)
        {
            if ((str == DBNull.Value) || (str == null))
                return def;
            else if (defIfEmpty && str.ToString() == "")
                return def;
            else
                return str.ToString();
        }

        /// <summary>
        /// Checks if current object has DBNull value assigned to
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public static bool IsNull(object obj)
        {
            if (obj == null) return true;

            if (obj == DBNull.Value)
                return true;
            else
                return false;
        }

        /// <summary>
        /// Checks whether text is numeric or not
        /// </summary>
        /// <param name="text"></param>
        /// <returns></returns>
        public static bool IsNumeric(string text, bool validateDecimal)
        {
            double tempDecimal = 0;
            if (validateDecimal)
                return Double.TryParse(text, NumberStyles.Any, CultureInfo.CurrentCulture.NumberFormat, out tempDecimal);
            else
                return Double.TryParse(text, NumberStyles.Integer, CultureInfo.CurrentCulture.NumberFormat, out tempDecimal);
        }

        /// <summary>
        /// Converts string to SQL string (properly escapes string to be used in a query)
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        public static string StringToSql(string input)
        {
            return StringToSql(input, 0);
        }
        public static string StringToSql(string input, int maxLen)
        {
            if (input != null)
            {
                if (maxLen > 0 && maxLen < input.Length) input = input.Substring(0, maxLen);
            }

            if (input != null)
                return input.Replace("'", "''");
            else
                return "";
        }

        /// <summary>
        /// Returns boolean to be used in sql query
        /// </summary>
        /// <param name="truefalse"></param>
        /// <returns></returns>
        public static int BitToSql(bool truefalse)
        {
            return truefalse ? 1 : 0;
        }

        /// <summary>
        /// Returns properly formatted double to be used in sql query. Returns 0 if value isn't decimal or is null.
        /// </summary>
        /// <param name="dbl"></param>
        /// <returns></returns>
        public static string DoubleToSql(string dbl)
        {
            return DoubleToSql(dbl, "0");
        }
        public static string DoubleToSql(string dbl, string def)
        {
            Double temp_dbl;
            if (!Double.TryParse(dbl, out temp_dbl)) return def;

            return DoubleToSql(temp_dbl);
        }

        /// <summary>
        /// Returns properly formatted double to be used in sql query
        /// </summary>
        public static string DoubleToSql(double dbl)
        {
            string ret = dbl.ToString();
            return ret;
        }

        /// <summary>
        /// Returns properly formatted decimal to be used in sql query. Returns 0 if value isn't decimal or is null.
        /// </summary>
        /// <param name="dec"></param>
        /// <returns></returns>
        public static string DecimalToSql(string dec)
        {
            return DecimalToSql(dec, "0");
        }
        public static string DecimalToSql(string dec, string def)
        {
            Decimal temp_dec;
            if (!Decimal.TryParse(dec, out temp_dec)) return def;

            return DecimalToSql(temp_dec);
        }

        /// <summary>
        /// Returns properly formatted decimal to be used in sql query
        /// </summary>
        /// <param name="dec"></param>
        /// <returns></returns>
        public static string DecimalToSql(decimal dec)
        {
            string ret = dec.ToString();
            ret = ret.Replace(",", ".");
            return ret;
        }

        /// <summary>
        /// Returns properly formatted decimal to be used in sql query
        /// </summary>
        /// <param name="dec"></param>
        /// <returns></returns>
        public static string DecimalToSql(decimal dec, string def)
        {
            return DecimalToSql(dec.ToString(), def);
        }

        /// <summary>
        /// Returns properly formatted datetime to be used in sql query. Returns as string with 'YYYYMMDD HHMM:SS.mm' or NULL if value is empty or MinDate.
        /// </summary>
        /// <param name="dt"></param>
        /// <returns></returns>
        public static string DateToSql(DateTime dt)
        {
            //if (dt.Year == DateTime.MinValue.Year && dt.Month == DateTime.MinValue.Month && dt.Day == DateTime.MinValue.Day)	// took 309 ms for 1000000 iterations
            if (dt.Ticks < TimeSpan.TicksPerDay) //took 8 ms for 1000000 iterations
                return "NULL";

            // 20060505 10:20:30.500'
            //return "'" + dt.Year.ToString("0000") + dt.Month.ToString("00") + dt.Day.ToString("00") + " " + dt.Hour.ToString("00") + ":" + dt.Minute.ToString("00") + ":" + dt.Second.ToString("00") + "." + dt.Millisecond.ToString("000") + "'";
            return "'" + dt.ToString("yyyyMMdd HH:mm:ss.fff") + "'";
        }

        /// <summary>
        /// Returns properly formatted string to be used in sql query. Returns as string with 'YYYYMMDD HHMM:SS.mm' or NULL if value is empty.
        /// </summary>
        /// <param name="s"></param>
        /// <returns></returns>
        public static string DateToSql(string s)
        {
            if (IsNull(s)) return "NULL";

            DateTime dt;
            if (!DateTime.TryParse(s, out dt)) return "NULL";

            return DateToSql(dt);
        }

        /// <summary>
        /// Returns integer as string (to be used in a sql query). Returns NULL if value is 0.
        /// </summary>
        /// <param name="value"></param>
        /// <param name="returnNULLIfValue"></param>
        /// <returns>NULL if value is 0, otherwise value is returned</returns>
        public static string IntToSql(int value)
        {
            return IntToSql(value, 0);
        }

        /// <summary>
        /// Returns integer as string (to be used in a sql query). Returns NULL if value is returnNULLIfValue.
        /// </summary>
        /// <param name="value"></param>
        /// <param name="returnNULLIfValue"></param>
        /// <returns></returns>
        public static string IntToSql(int value, int returnNULLIfValue)
        {
            if (value == returnNULLIfValue)
                return "NULL";
            else
                return value.ToString();
        }
        public static string IntToSql(int? value)
        {
            if (value.HasValue)
                return value.ToString();
            else
                return "NULL";
        }

        /// <summary>
        /// Converts sql column to integer. Returns 0 if column is NULL.
        /// </summary>
        /// <param name="column"></param>
        /// <returns></returns>
        public static int SqlToInt(object column)
        {
            return SqlToInt(column, 0);
        }

        /// <summary>
        /// Converts sql column to integer
        /// </summary>
        /// <param name="column"></param>
        /// <param name="valueIfNULL"></param>
        /// <returns></returns>
        public static int SqlToInt(object column, int valueIfNULL)
        {
            return CheckNum(column, valueIfNULL);
        }

        /// <summary>
        /// Converts sql column to string. Returns empty string if column is NULL.
        /// </summary>
        /// <param name="column"></param>
        /// <returns></returns>
        public static string SqlToString(object column)
        {
            return SqlToString(column, "", 0);
        }

        /// <summary>
        /// Converts sql column to string. Returns empty string if column is NULL.
        /// </summary>
        /// <param name="column"></param>
        /// <returns></returns>
        public static string SqlToString(object column, int maxLen)
        {
            return SqlToString(column, "", 0);
        }

        /// <summary>
        /// Converts sql column to string
        /// </summary>
        /// <param name="column"></param>
        /// <param name="valueIfNULL"></param>
        /// <returns></returns>
        public static string SqlToString(object column, string valueIfNULL)
        {
            return SqlToString(column, valueIfNULL, 0);
        }
        public static string SqlToString(object column, string valueIfNULL, int maxLen)
        {
            string val = "";
            if (IsNull(column))
                val = valueIfNULL;
            else
                val = column.ToString();

            if (maxLen > 0 && val.Length > maxLen) val = val.Substring(0, maxLen - 1);
            return val;
        }

        /// <summary>
        /// Converts sql column to decimal. 
        /// </summary>
        /// <param name="column"></param>
        /// <param name="valueIfNULL"></param>
        /// <returns></returns>
        public static Decimal SqlToDecimal(object column, Decimal valueIfNULL)
        {
            return CheckNumDecimal(column, valueIfNULL);
        }

        /// <summary>
        /// Converts sql column to decimal. Returns 0 if value is invalid.
        /// </summary>
        /// <param name="column"></param>
        /// <returns></returns>
        public static Decimal SqlToDecimal(object column)
        {
            return SqlToDecimal(column, 0);
        }

        /// <summary>
        /// Converts sql column to DateTime. 
        /// </summary>
        /// <param name="column"></param>
        /// <param name="valueIfNULL"></param>
        /// <returns></returns>
        public static DateTime SqlToDateTime(object column, DateTime valueIfNULL)
        {
            if (IsNull(column))
                return valueIfNULL;
            else
            {
                return (DateTime)column;
            }
        }

        /// <summary>
        /// Converts sql column to DateTime. Returns MinDate if value is NULL.
        /// </summary>
        /// <param name="column"></param>
        /// <returns></returns>
        public static DateTime SqlToDateTime(object column)
        {
            return SqlToDateTime(column, DateTime.MinValue);
        }

        /// <summary>
        /// Converts sql column to Boolean
        /// </summary>
        /// <param name="column"></param>
        /// <param name="valueIfNULL"></param>
        /// <returns></returns>
        public static bool SqlToBit(object column, bool valueIfNULL)
        {
            if (IsNull(column))
                return valueIfNULL;
            else
            {
                return (bool)column;
            }
        }

        /// <summary>
        /// Converts sql column to double
        /// </summary>
        /// <param name="column"></param>
        /// <param name="valueIfNULL"></param>
        /// <returns></returns>
        public static double SqlToDouble(object column, double valueIfNULL)
        {
            return CheckNumDouble(column, valueIfNULL);
        }

        /// <summary>
        /// Converts sql column to double. Returns 0 if value is NULL.
        /// </summary>
        /// <param name="column"></param>
        /// <returns></returns>
        public static double SqlToDouble(object column)
        {
            return SqlToDouble(column, 0);
        }


        /// <summary>
        /// Converts sql column to Boolean. Returns false if value is NULL.
        /// </summary>
        /// <param name="column"></param>
        /// <returns></returns>
        public static bool SqlToBit(object column)
        {
            return SqlToBit(column, false);
        }

        /// <summary>
        /// Returns true if entered word is valid for sql full text query. Valid means that is atleast 2 characters long (without stop characters)
        /// </summary>
        /// <param name="text"></param>
        /// <returns></returns>
        public static bool SqlFullTextValidString(string text)
        {
            string[] w = SqlFullTextWords(text);
            if (w != null)
                return true;

            return false;
        }

        /// <summary>
        /// Returns words that are full text safe
        /// </summary>
        /// <param name="text"></param>
        /// <returns></returns>
        public static string[] SqlFullTextWords(string text)
        {
            string badChars = "|&*[]\"()#$@,=<>!/\\~^-?%:;.";
            for (int x = 0; x <= badChars.Length - 1; x++)
            {
                while (text.IndexOf(badChars[x]) >= 0)
                    text = text.Remove(text.IndexOf(badChars[x]), 1);
            }

            while (text.IndexOf("  ") >= 0) text = text.Replace("  ", " ");
            text = text.Trim();

            if (text != "")
            {
                string[] w = text.Split(' ');
                if (w != null)
                {
                    List<string> words = new List<string>();
                    for (int x = 0; x <= w.Length - 1; x++)
                        if (w[x].Length > 2) words.Add(w[x]);

                    if (words.Count > 0) return words.ToArray();
                }
            }
            return null;
        }

        /// <summary>
        /// Creates SQL query that can be used with full text in where clause. { marko, mlakar } => "marko" AND "mlakar"
        /// Default operator: AND
        /// </summary>
        /// <param name="words"></param>
        /// <returns></returns>
        public static string SqlCreateFullTextQuery(string[] words)
        {
            return SqlCreateFullTextQuery(words, "AND");
        }

        /// <summary>
        /// Creates SQL query that can be used with full text in where clause. { marko, mlakar } => "marko" AND "mlakar"
        /// </summary>
        /// <param name="words">array of words</param>
        /// <param name="op">Operator used between words</param>
        /// <returns></returns>
        public static string SqlCreateFullTextQuery(string[] words, string op)
        {
            string sql = "";
            if (words != null)
            {
                for (int x = 0; x <= words.GetLength(0) - 1; x++)
                {
                    if (x > 0) sql += " " + op + " ";
                    sql += "\"" + StringToSql(words[x]) + "*\"";
                }
            }

            return sql;
        }

        /// <summary>
        /// Creates SQL query that can be used with full text in where clause. { marko, mlakar } => "marko" AND "mlakar"
        /// </summary>
        /// <param name="text"></param>
        /// <param name="op"></param>
        /// <returns></returns>
        public static string SqlCreateFullTextQuery(string text, string op)
        {
            return SqlCreateFullTextQuery(SqlFullTextWords(text), op);
        }

        /// <summary>
        /// Creates SQL query that can be used with full text in where clause. { marko, mlakar } => "marko" AND "mlakar"
        /// Default operator: AND
        /// </summary>
        /// <param name="text"></param>
        /// <returns></returns>
        public static string SqlCreateFullTextQuery(string text)
        {
            return SqlCreateFullTextQuery(text, "AND");
        }

        /// <summary>
        /// Represents Sql field as a string (can be used in sql "update .. set x=...")
        /// Sql field can be of any type and can also handle NULLs.
        /// </summary>
        /// <param name="field"></param>
        /// <returns></returns>
        public static string SqlFieldToString(object field)
        {
            if (field == null) return "NULL";

            string type = field.GetType().ToString();
            switch (type)
            {
                case "System.Int32":
                    return Convert.ToInt32(field).ToString();

                case "System.Int64":
                    return Convert.ToInt64(field).ToString();

                case "System.String":
                    return "N'" + StringToSql((string)field) + "'";

                case "System.DateTime":
                    return DateToSql((DateTime)field);

                case "System.Boolean":
                    return BitToSql((bool)field).ToString();

                case "System.Decimal":
                    return DecimalToSql((Decimal)field);

                case "System.DBNull":
                    return "NULL";

                case "System.Guid":
                    return "N'" + StringToSql(field.ToString()) + "'";

                default:
                    throw new Exception("SqlFieldToString(): type (" + type + ") not supported.");
            }
        }

        /// <summary>
        /// Verifies if string represents a boolean true. Funcions looks for: true, yes, 1, on
        /// </summary>
        /// <param name="text"></param>
        /// <returns></returns>
        public static bool VerifyTrue(string text)
        {
            if (IsNull(text)) return false;

            if ((text.ToLower() == "true") || (text.ToLower() == "yes") || (text == "1") || (text.ToLower() == "on"))
                return true;

            return false;
        }

        /// <summary>
        /// Verifies if string represents a boolean true. Funcions looks for: true, yes, 1, on
        /// </summary>
        /// <param name="text"></param>
        /// <returns></returns>
        public static bool VerifyTrue(object text)
        {
            if (IsNull(text)) return false;
            return VerifyTrue(text.ToString());
        }

        /// <summary>
        /// Verifies if string represents a boolean false. Funcions looks for: false, no, 0
        /// </summary>
        /// <param name="text"></param>
        /// <returns></returns>
        public static bool VerifyFalse(string text)
        {
            if (IsNull(text)) return false;

            if ((text.ToLower() == "false") || (text.ToLower() == "no") || (text == "0"))
                return true;

            return false;
        }

        /// <summary>
        /// Verifies if string represents a boolean false. Funcions looks for: false, no, 0
        /// </summary>
        /// <param name="text"></param>
        /// <returns></returns>
        public static bool VerifyFalse(object text)
        {
            if (IsNull(text)) return false;
            return VerifyFalse(text.ToString());
        }

        public class EmailValidator
        {

            bool invalid = false;
            static Regex _regex = new Regex(@"^(?("")("".+?(?<!\\)""@)|(([0-9a-z]((\.(?!\.))|[-!#\$%&'\*\+/=\?\^`\{\}\|~\w])*)(?<=[0-9a-z])@))" +
                          @"(?(\[)(\[(\d{1,3}\.){3}\d{1,3}\])|(([0-9a-z][-\w]*[0-9a-z]*\.)+[a-z0-9][\-a-z0-9]{0,22}[a-z0-9]))$", RegexOptions.IgnoreCase & RegexOptions.Compiled);


            public static bool Validate(string email)
            {
                EmailValidator eml = new EmailValidator();
                return eml.ValidateX(email.ToLowerInvariant());

            }

            private bool ValidateX(string strIn)
            {
                invalid = false;
                if (String.IsNullOrEmpty(strIn))
                    return false;

                // Use IdnMapping class to convert Unicode domain names. 

                strIn = Regex.Replace(strIn, @"(@)(.+)$", DomainMapper,
                                          RegexOptions.None);

                if (invalid)
                    return false;

                // Return true if strIn is in valid e-mail format. 

                return _regex.IsMatch(strIn);

            }

            private string DomainMapper(Match match)
            {
                // IdnMapping class with default property values.
                IdnMapping idn = new IdnMapping();

                string domainName = match.Groups[2].Value;
                try
                {
                    domainName = idn.GetAscii(domainName);
                }
                catch (ArgumentException)
                {
                    invalid = true;
                }
                return match.Groups[1].Value + domainName;
            }
        }

        /// <summary>
        /// Verify if email is correct
        /// </summary>
        /// <param name="email"></param>
        /// <param name="level">Level of verification, 1 - syntax only, 2 - syntax + verify domain</param>
        /// <returns></returns>
        public static bool VerifyEmail(string email, int level)
        {
            bool ok = false;
            if (email == null) return false;
            email = email.Trim();
            if (email == "") return false;

            return EmailValidator.Validate(email);

            /*
			string strRegex = @"^([a-zA-Z0-9_\-\.]+)@((\[[0-9]{1,3}" +
					 @"\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([a-zA-Z0-9\-]+\" +
					 @".)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$";
			Regex re = new Regex(strRegex);
			ok = re.IsMatch(email);

			if (ok && (email.Contains("..") || email.Contains(".@"))) ok = false;

			if (level <= 1) return ok;
			if (!ok) return false;

			string[] parts = email.Split('@');
			try { IPHostEntry IPhst = Dns.GetHostEntry(parts[1]); }
			catch { return false; }
			
			return true;
			*/
        }

        /// <summary>
        /// Verify if email is correct
        /// </summary>
        /// <param name="email"></param>
        /// <returns></returns>
        public static bool VerifyEmail(string email)
        {
            return VerifyEmail(email, 1);
        }

        /// <summary>
        /// Verifies one or more emails, separated by , or ;
        /// </summary>
        /// <param name="emails"></param>
        /// <param name="level">Level of verification, 1 - syntax only, 2 - syntax + verify domain</param>
        /// <param name="firstBadEmail"></param>
        /// <returns></returns>
        public static bool VerifyEmails(string emails, int level, ref string firstBadEmail)
        {
            emails = CheckStr(emails).Trim().Replace(",", ";");
            string[] tmp = emails.Split(';');
            for (int x = 0; x <= tmp.Length - 1; x++)
            {
                if (!VerifyEmail(tmp[x].Trim(), level))
                {
                    firstBadEmail = tmp[x].Trim();
                    return false;
                }
            }

            return true;
        }

        /// <summary>
        /// Verifies one or more emails, separated by , or ;
        /// </summary>
        /// <param name="emails"></param>
        /// <param name="level">Level of verification, 1 - syntax only, 2 - syntax + verify domain</param>
        /// <returns></returns>
        public static bool VerifyEmails(string emails, int level)
        {
            string tmp = "";
            return VerifyEmails(emails, level, ref tmp);
        }

        /// <summary>
        /// Verifies one or more emails, separated by , or ;
        /// </summary>
        /// <param name="emails"></param>
        /// <returns></returns>
        public static bool VerifyEmails(string emails)
        {
            string tmp = "";
            return VerifyEmails(emails, 1, ref tmp);
        }

        public static bool IsEmail(string email)
        {
            var retVal = false;
            if (email.Length > 0)
            {
                Regex regex = new Regex(@"^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,4})+$");
                Match match = regex.Match(email);
                if (match.Success)
                {
                    retVal = true;
                }
            }
            return retVal;
        }

        public static bool SendToDeveloper(string subject, string body)
        {
            string emails = GetSettings("a8PageErrorHandlerRecipients");
            if (emails.Length > 0)
            {
                return SendToDeveloper(emails, subject, body);
            }
            return false;
        }

        public static bool SendToDeveloper(string emails, string subject, string body)
        {
            List<System.Net.Mail.MailAddress> recipients = new List<System.Net.Mail.MailAddress>();
            if (emails.Length > 0)
            {
                recipients = A8.Email.CreateRecipients(emails);
            }
            if (recipients.Count > 0)
            {
                string sender = GetSettings("a8PageErrorHandlerSender");
                if (sender.Length == 0) sender = recipients[0].Address;
                A8.Email.SendHtmlEmail(new System.Net.Mail.MailAddress(sender), recipients, subject, body);
                return true;
            }
            return false;
        }

        public static string GetSettings(string webConfigKey)
        {
            return A8.Common.CheckStr(System.Configuration.ConfigurationManager.AppSettings[webConfigKey]);
        }

        /// <summary>
        /// Normalizes string so that it can be used in url.
        /// </summary>
        /// <param name="url"></param>
        /// <param name="replacement">this is the char we replace evil char with</param>
        /// <returns></returns>
        public static string StringToUrlSafe(string url, char replacement)
        {
            string safeCharacters = "1234567890-qwertzuiopasdfghjklyxcvbnmQWERTZUIOPASDFGHJKLYXCVBNM";

            url = url.Replace("š", "s");
            url = url.Replace("đ", "dz");
            url = url.Replace("č", "c");
            url = url.Replace("ć", "c");
            url = url.Replace("ž", "z");

            url = url.Replace("Š", "S");
            url = url.Replace("Đ", "DZ");
            url = url.Replace("Č", "C");
            url = url.Replace("Ć", "C");
            url = url.Replace("Ž", "Z");

            url = url.Replace("ü", "u"); url = url.Replace("Ü", "U");
            url = url.Replace("ö", "o"); url = url.Replace("Ö", "O");
            url = url.Replace("ä", "a"); url = url.Replace("Ä", "A");
            url = url.Replace("ß", "s");

            for (int x = 0; x <= url.Length - 1; x++) if (!safeCharacters.Contains(url[x].ToString())) url = url.Replace(url[x], replacement);

            while (url.Contains("--")) url = url.Replace("--", "-");
            if (url.StartsWith("-")) url = url.Substring(1);
            if (url.EndsWith("-") && url.Length > 2) url = url.Substring(0, url.Length - 1);
            return url.ToLower();
        }


        public static string SafeFieldName(string text)
        {
            string safeCharacters = "1234567890qwertzuiopasdfghjklyxcvbnmQWERTZUIOPASDFGHJKLYXCVBNM-_";

            text = text.Replace("š", "s");
            text = text.Replace("đ", "dz");
            text = text.Replace("č", "c");
            text = text.Replace("ć", "c");
            text = text.Replace("ž", "z");

            text = text.Replace("Š", "S");
            text = text.Replace("Đ", "DZ");
            text = text.Replace("Č", "C");
            text = text.Replace("Ć", "C");
            text = text.Replace("Ž", "Z");

            text = text.Replace("ü", "u"); text = text.Replace("Ü", "U");
            text = text.Replace("ö", "o"); text = text.Replace("Ö", "O");
            text = text.Replace("ä", "a"); text = text.Replace("Ä", "A");
            text = text.Replace("ß", "s");

            for (int x = 0; x <= text.Length - 1; x++) if (!safeCharacters.Contains(text[x].ToString())) text = text.Replace(text[x], '-');

            while (text.Contains("--")) text = text.Replace("--", "-");
            if (text.StartsWith("-")) text = text.Substring(1);
            if (text.EndsWith("-") && text.Length > 2) text = text.Substring(0, text.Length - 1);
            return text.Replace("-", "_");
        }


        /// <summary>
        /// Normalizes string so that it can be used in url. Evil characters are replaced with -.
        /// </summary>
        /// <param name="url"></param>
        /// <returns></returns>
        public static string StringToUrlSafe(string url)
        {
            return StringToUrlSafe(url, '-');
        }

        /// <summary>
        /// Replaces all occurances of new lines to break tag. (&lt;br /&gt;)
        /// </summary>
        /// <param name="text"></param>
        /// <returns></returns>
        public static string NewLineToBreak(string text)
        {
            text = text.Replace("\n", "<br />");
            text = text.Replace("\r", string.Empty);
            return text;
        }

        /// <summary>
        /// Updates query string parameters. Parameters, that have value null or empty string, are removed.
        /// </summary>
        /// <param name="url"></param>
        /// <param name="updateParameters"></param>
        /// <returns></returns>
        public static string UpdateQueryParameters(string url, Hashtable updateParameters)
        {
            return UpdateQueryParameters(url, updateParameters, true);
        }

        /// <summary>
        /// Updates query string parameters.
        /// </summary>
        /// <param name="url"></param>
        /// <param name="updateParameters"></param>
        /// <param name="removeEmpty">if true, parameters, that have value null or empty string, are removed.</param>
        /// <returns></returns>
        public static string UpdateQueryParameters(string url, Hashtable updateParameters, bool removeEmpty)
        {
            if (updateParameters == null || updateParameters.Count == 0) return url;
            string path = url;
            string queryString = "";
            string anchor = "";
            if (url.Contains("#"))
            {
                string[] urlParts = url.Split('#');
                if (urlParts != null && urlParts.Length >= 2)
                {
                    path = urlParts[0];
                    anchor = urlParts[1];
                }
            }
            {
                string[] urlParts = path.Split('?');
                if (urlParts != null && urlParts.Length == 2)
                {
                    path = urlParts[0];
                    queryString = urlParts[1];
                }
            }
            NameValueCollection parameters = HttpUtility.ParseQueryString(queryString);
            foreach (DictionaryEntry e in updateParameters)
            {
                string key = Common.CheckStr(e.Key);
                string value = (e.Value == null) ? null : Common.CheckStr(e.Value);
                if (((value == null) || (value == "")) && (removeEmpty))
                {
                    parameters.Remove(key);
                }
                else
                {
                    parameters[key] = value;
                }
            }
            queryString = parameters.ToString();
            string newUrl = path;
            {
                if ((queryString != null) && (queryString != "")) newUrl += "?" + queryString;
                if ((anchor != null) && (anchor != "")) newUrl += "#" + anchor;
            }
            return newUrl;
        }
        public static string UpdateQueryParameters(string url, NameValueCollection updateParameters, bool removeEmpty)
        {
            Hashtable parameters = new Hashtable();
            foreach (string key in updateParameters.AllKeys)
            {
                parameters.Add(key, updateParameters[key]);
            }
            return UpdateQueryParameters(url, parameters, removeEmpty);
        }
        public static string UpdateQueryParameters(string url, string updateParameters, bool removeEmpty)
        {
            return UpdateQueryParameters(url, HttpUtility.ParseQueryString(updateParameters), removeEmpty);
        }

        /// <summary>
        /// Updates a single query parameter for a specified url
        /// </summary>
        /// <param name="url"></param>
        /// <param name="parameter"></param>
        /// <param name="value">if value is null or empty, parameter is removed.</param>
        /// <returns></returns>
        public static string UpdateQueryParameter(string url, string parameter, string value)
        {
            return UpdateQueryParameter(url, parameter, value, true);
        }

        /// <summary>
        /// Updates a single query parameter for a specified url
        /// </summary>
        /// <param name="url"></param>
        /// <param name="parameter"></param>
        /// <param name="value"></param>
        /// <param name="removeEmpty">if true and value is null or empty string, parameter is removed.</param>
        /// <returns></returns>
        public static string UpdateQueryParameter(string url, string parameter, string value, bool removeEmpty)
        {
            //if (((value == null) || (value == "")) && (removeEmpty)) return RemoveQueryParameter(url, parameter);
            Hashtable parameters = new Hashtable();
            parameters[parameter] = value;
            return UpdateQueryParameters(url, parameters, removeEmpty);
        }

        /// <summary>
        /// Removes a query parameter from a specified url
        /// </summary>
        /// <param name="url"></param>
        /// <param name="parameter"></param>
        /// <returns></returns>
        public static string RemoveQueryParameter(string url, string parameter)
        {
            return UpdateQueryParameter(url, parameter, null, true);
        }

        /// <summary>
        /// Returns MD5 hash
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        public static string Md5Hash(string input)
        {
            MD5 md5Hasher = MD5.Create();
            byte[] data = md5Hasher.ComputeHash(Encoding.Default.GetBytes(Common.CheckStr(input)));
            StringBuilder sBuilder = new StringBuilder();
            for (int i = 0; i < data.Length; i++) sBuilder.Append(data[i].ToString("x2"));
            return sBuilder.ToString();
        }

        /// <summary>
        /// Represents the number as a string with leading 0. 
        /// </summary>
        /// <param name="num">number</param>
        /// <param name="len">the final length of the number (0 included)</param>
        /// <returns></returns>
        public static string LeadingZero(int num, int len)
        {
            string ret = num.ToString();
            for (int x = 1; x <= len - num.ToString().Length; x++)
                ret = "0" + ret;

            return ret;
        }

        /// <summary>
        /// Generates random string (case insensitive + numbers)
        /// </summary>
        /// <param name="len"></param>
        /// <returns></returns>
        public static string RandomString(int len)
        {
            return RandomString(len, true, true, true, true);
        }

        /// <summary>
        /// Generates random string
        /// </summary>
        /// <param name="len"></param>
        /// <param name="lowerLetters">include lower letters</param>
        /// <param name="upperLetters">include upper letters</param>
        /// <param name="numbers">include numbers</param>
        /// <returns></returns>
        public static string RandomString(int len, bool lowerLetters, bool upperLetters, bool numbers, bool special)
        {
            string charsLetters = "qwertzuiopasdfghjklyxcvbnm";
            string charsNumbers = "1234567890";
            string charsSpecial = "!#$%&?";
            string charsAvailable = "";
            string ret = "";

            if (randomObject == null) randomObject = new Random();

            if (len > 0)
            {
                if (lowerLetters) charsAvailable += charsLetters;
                if (upperLetters) charsAvailable += charsLetters.ToUpper();
                if (numbers) charsAvailable += charsNumbers;
                if (special) charsAvailable += charsSpecial;

                if (charsAvailable.Length == 0)
                    throw new Exception("For RandomString() to work atleast one of the character types needs to be specified.");

                for (int x = 1; x <= len; x++)
                    ret += charsAvailable.Substring(randomObject.Next(0, charsAvailable.Length), 1);
            }
            else
            {
                throw new Exception("RandomString requires len>0");
            }

            return ret;
        }

        public static int RandomNum(int min, int max)
        {
            if (randomObject == null) randomObject = new Random();
            return randomObject.Next(min, max);
        }

        /// <summary>
        /// Shuffle generic List
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="list"></param>
        public static void Shuffle<T>(this IList<T> list)
        {
            if (randomObject == null) randomObject = new Random();

            int n = list.Count;
            while (n > 1)
            {
                n--;
                int k = randomObject.Next(n + 1);
                T value = list[k];
                list[k] = list[n];
                list[n] = value;
            }
        }
        /*
            List<Product> products = GetProducts();
            products.Shuffle();
        */

        /// <summary>
        /// Formats datetime as a string. 
        /// </summary>
        /// <param name="date"></param>
        /// <param name="format">Any string, built in tags: dd (day), %d (single digit day), mm (month), %m (single digit month), yyyy (year), %y (2 digit year), hh (hour), %h (single digit hour), nn (minute), %n (single digit minute), ss (seconds), %s (single digit seconds)</param>
        /// <returns></returns>
        public static string FormatDateTime(DateTime date, string format)
        {
            format = format.Replace("dd", LeadingZero(date.Day, 2));
            format = format.Replace("%d", date.Day.ToString());
            format = format.Replace("mm", LeadingZero(date.Month, 2));
            format = format.Replace("%m", date.Month.ToString());
            format = format.Replace("yyyy", date.Year.ToString());
            format = format.Replace("%y", (date.Year > 1000 ? date.Year.ToString().Substring(2) : date.Year.ToString()));
            format = format.Replace("hh", LeadingZero(date.Hour, 2));
            format = format.Replace("%h", date.Hour.ToString());
            format = format.Replace("nn", LeadingZero(date.Minute, 2));
            format = format.Replace("%n", date.Minute.ToString());
            format = format.Replace("ss", LeadingZero(date.Second, 2));
            format = format.Replace("%s", date.Second.ToString());

            return format;
        }

        public static string FormatMoney(decimal value)
        {
            return String.Format("{0:N2}", CheckNumDecimal(value));
        }

        /// <summary>
        /// Formats current date (datetime) as a string. 
        /// </summary>
        /// <param name="format">Any string, built in tags: dd (day), mm (month), yyyy (year), %y (2-digit year), hh (hour), nn (minutes), ss (seconds)</param>
        /// <returns></returns>
        public static string FormatDateTime(string format)
        {
            return FormatDateTime(DateTime.Now, format);
        }

        /// <summary>
        /// Returns date as string in dd.mm.yyyy
        /// </summary>
        /// <param name="date"></param>
        /// <returns></returns>
        public static string ShortDate(DateTime date)
        {
            return FormatDateTime(date, "%d. %m. yyyy");
        }

        /// <summary>
        /// Returns date as string in dd.mm.%y
        /// </summary>
        /// <param name="date"></param>
        /// <returns></returns>
        public static string ShorterDate(DateTime date)
        {
            return FormatDateTime(date, "%d. %m. %y");
        }

        /// <summary>
        /// Returns date as string in dd.mm if year is current year, otherwise dd.mm.%y
        /// </summary>
        /// <param name="date"></param>
        /// <returns></returns>
        public static string ShortestDate(DateTime date)
        {
            return ShortestDate(date, true);
        }
        public static string ShortestDate(DateTime date, bool cutLeadingZero)
        {
            string day = "dd";
            string month = "mm";
            if (cutLeadingZero) { day = "%d"; month = "%m"; }

            if (DateTime.Now.Year != date.Year)
                return FormatDateTime(date, day + "." + month + ".%y");
            else
                return FormatDateTime(date, day + "." + month + ".");
        }

        /// <summary>
        /// Returns datetime as string in dd.mm.yyyy hh:nn:ss
        /// </summary>
        /// <returns></returns>
        public static string ShortDateTime()
        {
            return ShortDateTime(DateTime.Now);
        }

        /// <summary>
        /// Returns datetime as string in dd.mm.yyyy hh:nn
        /// </summary>
        /// <param name="date"></param>
        /// <returns></returns>
        public static string ShortDateTime(Object date)
        {
            if ((date == null) || (IsNull(date)))
                return "n/a";
            else
                return FormatDateTime((DateTime)date, "%d. %m. yyyy hh:nn:ss");
        }

        /// <summary>
        /// Converts Unix time to DateTime.
        /// </summary>
        /// <param name="unixTime"></param>
        /// <returns></returns>
        public static DateTime UnixTimeToDateTime(double unixTime)
        {
            DateTime origin = new DateTime(1970, 1, 1, 0, 0, 0, 0);
            return origin.AddSeconds(unixTime);
        }

        /// <summary>
        /// Converts DateTime to Unix time (timestamp)
        /// </summary>
        /// <param name="dateTime"></param>
        /// <returns></returns>
        public static double DateTimeToUnixTime(DateTime dateTime)
        {
            DateTime origin = new DateTime(1970, 1, 1, 0, 0, 0, 0);
            TimeSpan diff = dateTime - origin;
            return Math.Floor(diff.TotalSeconds);
        }

        /// <summary>
        /// Validates date against few different formats (9.12.03, 9.12.2003, 9.12.2003 10:30, ...)
        /// </summary>
        /// <param name="datetime"></param>
        /// <param name="defValue"></param>
        /// <returns></returns>
        public static DateTime CheckDateTime(string datetime, DateTime defValue)
        {
            if (String.IsNullOrEmpty(datetime)) return defValue;

            string trimmedDateTime = datetime.Trim();


            DateTime dt = Common.ParseDateTime(trimmedDateTime, "%d.%m.%y"); // 9.12.03
            if (dt != DateTime.MinValue) return dt;

            dt = Common.ParseDateTime(trimmedDateTime, "%d.%m.%y %H:%n"); // 9.12.03 10:30
            if (dt != DateTime.MinValue) return dt;

            dt = Common.ParseDateTime(trimmedDateTime, "%d.%m.%y %H:%n:%s"); // 9.12.03 10:30:10
            if (dt != DateTime.MinValue) return dt;

            dt = Common.ParseDateTime(trimmedDateTime, "%d.%m.yyyy"); // 9.3.2003
            if (dt != DateTime.MinValue) return dt;

            dt = Common.ParseDateTime(trimmedDateTime, "%d.%m.yyyy %H:%n"); // 9.12.2003 10:30
            if (dt != DateTime.MinValue) return dt;

            dt = Common.ParseDateTime(trimmedDateTime, "%d.%m.yyyy %H:%n:%s"); // 9.12.2003 10:30:10
            if (dt != DateTime.MinValue) return dt;

            dt = Common.ParseDateTime(trimmedDateTime, "%d.%m."); // 9.12.
            if (dt != DateTime.MinValue) return dt;

            dt = Common.ParseDateTime(trimmedDateTime, "%d.%m"); // 9.12
            if (dt != DateTime.MinValue) return dt;

            dt = Common.ParseDateTime(trimmedDateTime, "yyyy-%m-%d"); // 2014-01-13
            if (dt != DateTime.MinValue) return dt;

            return defValue;
        }

        /// <summary>
        /// Parses and validates the input string. If invalid, returns DateTime.MinValue
        /// </summary>
        /// <param name="input"></param>
        /// <param name="format">Format of the input string. dd (day), %d (single digit day), mm (month), %m (single digit month), yyyy (year), %y (2 digit year), hh (hour), %h (single digit hour), nn (minute), %n (single digit minute), ss (seconds), %s (single digit seconds)</param>
        /// <returns></returns>
        public static DateTime ParseDateTime(string input, string format)
        {
            // We need to adjust the parameters a little so we keep compatible format throughout the sitekit (FormatDateTime)
            format = format.Replace("mm", "MM");
            format = format.Replace("%m", "%M");
            format = format.Replace("%n", "%m");
            format = format.Replace("hh", "HH");
            format = format.Replace("nn", "mm");

            DateTime dt;
            try
            {
                dt = DateTime.ParseExact(input, format, CultureInfo.CurrentCulture);
            }
            catch
            {
                return DateTime.MinValue;
            }

            return dt;
        }

        /// <summary>
        /// Lighter version
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public static DateTime DateTimeParse(string value)
        {
            DateTime result;
            if (!DateTime.TryParse(value, out result)) { return DateTime.MinValue; }
            return result;
        }

        /// <summary>
        /// Parses a date specified as a string dd.mm.yyy. If invalid, returns DateTime.MinValue
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        public static DateTime ParseDate(string input)
        {
            return ParseDateTime(input, "dd.mm.yyyy");
        }

        /// <summary>
        /// Parses a date specified as a string dd.mm.yyyy hh:ii. If invalid, returns DateTime.MinValue
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        public static DateTime ParseDateTime(string input)
        {
            return ParseDateTime(input, "dd.mm.yyyy hh:nn");
        }

        /// <summary>
        /// Compares two datetimes. The smallest element that we're comparing is a second.
        /// </summary>
        /// <param name="t1"></param>
        /// <param name="t2"></param>
        /// <returns></returns>
        public static bool DateTimesEqual(DateTime t1, DateTime t2)
        {
            return (t1.Day == t2.Day) && (t1.Month == t2.Month) && (t1.Year == t2.Year) && (t1.Hour == t2.Hour) && (t1.Minute == t2.Minute) && (t1.Second == t2.Second);
        }

        /// <summary>
        /// Date as string with prefix and sufix
        /// </summary>
        /// <param name="dateFrom"></param>
        /// <param name="dateTo"></param>
        /// <param name="addTranslations"></param>
        /// <returns></returns>
        public static string DateFromToAsString(DateTime dateFrom, DateTime dateTo, bool addTranslations)
        {
            return DateFromToAsString(dateFrom, dateTo, null, null, addTranslations);
        }

        public static string DateFromToAsString(DateTime dateFrom, DateTime dateTo, TimeSpan? timeFrom, TimeSpan? timeTo, bool addTranslations)
        {
            string retVal = string.Empty;

            string datePart = string.Empty;
            string timePart = string.Empty;

            // dealing with time
            if (timeFrom.HasValue && timeTo.HasValue && (timeFrom.Value > TimeSpan.Zero || timeTo.Value > TimeSpan.Zero))
            {
                if (timeFrom > TimeSpan.Zero && timeTo > TimeSpan.Zero)
                {
                    if (timeFrom.Value == timeTo.Value)
                    {
                        timePart = timeFrom.Value.ToString("hh\\:mm");
                    }
                    else
                    {
                        if (addTranslations)
                        {
                            timePart = A8.CommonUmbraco.Translate("general.time_from").ToLower() + " " + timeFrom.Value.ToString("hh\\:mm") + " " + A8.CommonUmbraco.Translate("general.time_to").ToLower() + " " + timeTo.Value.ToString("hh\\:mm");
                        }
                        else
                        {
                            timePart = timeFrom.Value.ToString("hh\\:mm") + " &rarr; " + timeTo.Value.ToString("hh\\:mm");
                        }
                    }
                }
                else if (timeFrom.Value == TimeSpan.Zero && timeTo.Value > TimeSpan.Zero)
                {
                    if (addTranslations)
                    {
                        timePart = A8.CommonUmbraco.Translate("general.time_to").ToLower() + " " + timeTo.Value.ToString("hh\\:mm");
                    }
                    else
                    {
                        timePart = "&rarr; " + timeTo.Value.ToString("hh\\:mm");
                    }
                }
                else if (timeFrom.Value > TimeSpan.Zero && timeTo.Value == TimeSpan.Zero)
                {
                    if (addTranslations)
                    {
                        timePart = A8.CommonUmbraco.Translate("general.time_from").ToLower() + " " + timeFrom.Value.ToString("hh\\:mm");
                    }
                    else
                    {
                        timePart = timeFrom.Value.ToString("hh\\:mm") + " &rarr;";
                    }
                }
            }

            // dealing with date
            if (dateFrom.Date > DateTime.MinValue.Date && dateTo.Date < DateTime.MaxValue.Date)
            {
                if (dateFrom.Date == dateTo.Date)
                {
                    datePart = dateFrom.ToString("%d. %M. yyyy");
                }
                else
                {
                    if (addTranslations)
                    {
                        datePart = A8.CommonUmbraco.Translate("general.date_from").ToFirstUpper() + " " + dateFrom.ToString("%d. %M. yyyy") + " " + A8.CommonUmbraco.Translate("general.date_to").ToLower() + " " + dateTo.ToString("%d. %M. yyyy");
                    }
                    else
                    {
                        datePart = dateFrom.ToString("%d. %M. yyyy") + " &rarr; " + dateTo.ToString("%d. %M. yyyy");
                    }
                }
            }
            else if (dateFrom.Date == DateTime.MinValue.Date && dateTo.Date < DateTime.MaxValue.Date)
            {
                if (addTranslations)
                {
                    datePart = A8.CommonUmbraco.Translate("general.date_to").ToFirstUpper() + " " + dateTo.ToString("%d. %M. yyyy");
                }
                else
                {
                    datePart = "&rarr; " + dateTo.ToString("%d. %M. yyyy");
                }
            }
            else if (dateFrom.Date > DateTime.MinValue.Date && dateTo.Date == DateTime.MaxValue.Date)
            {
                if (addTranslations)
                {
                    datePart = A8.CommonUmbraco.Translate("general.date_from").ToFirstUpper() + " " + dateFrom.ToString("%d. %M. yyyy");
                }
                else
                {
                    datePart = dateFrom.ToString("%d. %M. yyyy") + " &rarr;";
                }
            }

            // return all combined
            return datePart + (timePart.Length > 0 ? ", " + timePart : string.Empty);
        }

        public static string FormatDateUtc(DateTime d)
        {
            TimeSpan utcOffset = TimeZone.CurrentTimeZone.GetUtcOffset(DateTime.Now);
            return String.Format("{0:yyyy-MM-ddTHH\\:mm\\:ss}", d) + ((utcOffset < TimeSpan.Zero) ? "-" : "+") + String.Format("{0:hh\\:mm}", utcOffset);
        }

        /// <summary>
        /// Shortens a string
        /// </summary>
        /// <param name="str"></param>
        /// <param name="max"></param>
        /// <param name="mustBeVisiblePart">left (default), right, center or leftright</param>
        /// <param name="autoCloseTags">Close unclosed (shortened) tags automatically</param>
        /// <returns></returns>
        public static string ShortenString(string str, int max, string mustBeVisiblePart)
        {
            return ShortenString(str, max, mustBeVisiblePart, true);
        }

        /// <summary>
        /// Closes open tags.
        /// </summary>
        /// <param name="html"></param>
        /// <returns></returns>
        public static string CloseOpenTags(string html)
        {
            List<string> stack = new List<string>();

            string t = "";
            html = html.Replace("<br>", "<br />");
            int start = html.IndexOf("<");
            int offset = 0;
            bool atleastOneClosed = false;
            while (start >= 0)
            {
                int end = html.IndexOf(">", start);
                if (end > start)
                {
                    atleastOneClosed = true;
                    string tag = html.Substring(start + 1, end - start - 1).Trim();
                    if (tag.Contains(" ")) tag = tag.Substring(0, tag.IndexOf(" "));

                    if (!tag.StartsWith("/"))
                        stack.Add(tag);
                    else
                    {
                        if (stack.Count > 0)
                            stack.RemoveAt(stack.Count - 1);
                    }
                }
                if (!atleastOneClosed)
                {
                    string tag = html.Substring(start + 1).Trim();
                    if (tag.Contains(" ")) tag = tag.Substring(0, tag.IndexOf(" "));
                    if (!tag.StartsWith("/")) stack.Add(tag);
                }
                offset = start + 1;
                start = html.IndexOf("<", offset);
            }

            if (stack != null && stack.Count > 0)
            {
                for (int x = stack.Count - 1; x >= 0; x--) html = html + "</" + stack[x] + ">";
            }
            return html;
        }

        /// <summary>
        /// Shortens a string
        /// </summary>
        /// <param name="str"></param>
        /// <param name="max"></param>
        /// <param name="mustBeVisiblePart">left (default), right, center or leftright</param>
        /// <param name="autoCloseTags">Close open (shortened) tags automatically</param>
        /// <returns></returns>
        public static string ShortenString(string str, int max, string mustBeVisiblePart, bool autoCloseTags)
        {
            if ((str.Length > max) && (str.Length > 4) && (max > 4))
            {
                string p1 = "";
                string p2 = "";

                switch (mustBeVisiblePart.ToLower())
                {
                    case "left":
                    default:
                        p1 = str.Substring(0, max - 3);
                        if (autoCloseTags) p1 = CloseOpenTags(p1);
                        return p1 + "...";

                    case "right":
                        p1 = str.Substring(str.Length - max + 3);
                        if (autoCloseTags) p1 = CloseOpenTags(p1);
                        return "..." + p1;

                    case "center":
                        int padding = (str.Length - max) / 2;
                        p1 = str.Substring(padding, str.Length - (2 * padding));
                        if (autoCloseTags) p1 = CloseOpenTags(p1);
                        return "..." + p1 + "...";

                    case "leftright":
                        int partLen = (max / 2);
                        p1 = str.Substring(0, partLen - 2);
                        p2 = str.Substring(str.Length - partLen + 1);

                        if (autoCloseTags)
                        {
                            p1 = CloseOpenTags(p1);
                            p2 = CloseOpenTags(p2);
                        }
                        return p1 + "..." + p2;
                }
            }
            else
                return str;
        }

        /// <summary>
        /// Shortens a string to N characters (first N characters are displayed)
        /// </summary>
        /// <param name="str"></param>
        /// <param name="max"></param>
        /// <returns></returns>
        public static string ShortenString(string str, int max)
        {
            return ShortenString(str, max, "left");
        }

        /// <summary>
        /// Detags a html string.
        /// </summary>
        /// <param name="html"></param>
        /// <returns></returns>
        /// 
        public static string Detag(string html)
        {
            return Detag(html, string.Empty);
        }
        public static string Detag(string html, string replaceWith)
        {
            if (html == null) return "";

            string stripped = html.Trim();
            if (stripped != "")
                stripped = Regex.Replace(stripped, @"<(.|\n)*?>", replaceWith);

            return stripped;
        }

        public static string HtmlEncodeX(string text)
        {
            if (text.Length > 0)
            {
                // yyyy-mm-dd
                char[] chars = HttpUtility.HtmlEncode(text).ToCharArray();
                StringBuilder result = new StringBuilder(text.Length + (int)(text.Length * 0.1));
                foreach (char c in chars)
                {
                    int value = Convert.ToInt32(c);
                    if (value > 127)
                        result.AppendFormat("&#{0};", value);
                    else
                        result.Append(c);
                }
                return result.ToString();
            }
            else
            {
                return string.Empty;
            }
        }

        /// <summary>
        /// Returns query parameter taken from Request.Form (first) or Request.QueryString
        /// </summary>
        /// <param name="p"></param>
        /// <returns></returns>
        public static string RequestAny(string p)
        {
            if (HttpContext.Current.Request.Form[p] != null)
                return HttpContext.Current.Request.Form[p];
            else
            {
                if (HttpContext.Current.Request.QueryString[p] != null)
                {
                    return HttpContext.Current.Request.QueryString[p];
                }
                else
                    return "";
            }
        }

        /// <summary>
        /// Returns query parameter as a integer
        /// </summary>
        /// <param name="p"></param>
        /// <param name="defaultValue">If query is not string, use this number</param>
        /// <returns></returns>
        public static int RequestAnyInt(string p, int defaultValue)
        {
            return CheckNum(RequestAny(p), defaultValue);
        }

        /// <summary>
        /// Returns query parameter as a integer. 0 if not number.
        /// </summary>
        /// <param name="p"></param>
        /// <returns></returns>
        public static int RequestAnyInt(string p)
        {
            return RequestAnyInt(p, 0);
        }

        /// <summary>
        /// Returns query parameter as boolean. True if 1, true or yes.
        /// </summary>
        /// <param name="p"></param>
        /// <returns></returns>
        public static bool RequestAnyBool(string p)
        {
            return VerifyTrue(RequestAny(p));
        }

        public static string ShortenWholeWord(string str, int max)
        {
            return ShortenWholeWord(str, max, false);
        }

        public static string ShortenWholeWord(string str, int max, bool noElipsys)
        {
            if (str.Length >= max)
            {
                str += " ";
                str = str.Substring(0, max);
                if (str.LastIndexOf(" ") > 0)
                    str = str.Substring(0, str.LastIndexOf(" "));
                if (!noElipsys)
                {
                    str += "...";
                }
            }
            return str;
        }

        public static string BreakLongString(string stringToBreak, int chunkLength)
        {
            string newString = String.Empty;
            if (stringToBreak.Length > 0)
            {
                int counter = 0;
                string tmpString = string.Empty;
                for (int j = 0; j < stringToBreak.Length; j++)
                {
                    tmpString = stringToBreak.Substring(j, 1);
                    newString += tmpString;
                    if (tmpString == " " || tmpString == "-")
                    {
                        counter = 0;
                    }
                    else
                    {
                        counter++;
                    }
                    if (counter >= chunkLength)
                    {
                        newString += " ";
                        counter = 0;
                    }
                }
            }
            return newString;
        }

        /// <summary>
        /// Removes Http or Https from url
        /// </summary>
        /// <param name="url"></param>
        /// <returns></returns>
        public static string StripHttpProtocol(string url)
        {
            if (url.StartsWith("http://") || url.StartsWith("https://"))
            {
                url = url.Replace("http://", "").Replace("https://", "").Trim();
            }

            if (!url.StartsWith("//"))
            {
                url = "//" + url;
            }

            if (!url.EndsWith("/"))
            {
                url += "/";
            }
            return url;
        }

        public static string CryptKey
        {
            //key must have 16 chars
            get { return "jfkgotmyvhspcandxlrwebquiz"; }
        }

        public static string Encrypt(string clearText, string encryptionKey)
        {
            byte[] clearBytes = Encoding.Unicode.GetBytes(clearText);
            using (Aes encryptor = Aes.Create())
            {
                Rfc2898DeriveBytes pdb = new Rfc2898DeriveBytes(encryptionKey, new byte[] { 0x49, 0x76, 0x61, 0x6e, 0x20, 0x4d, 0x65, 0x64, 0x76, 0x65, 0x64, 0x65, 0x76 });
                encryptor.Key = pdb.GetBytes(32);
                encryptor.IV = pdb.GetBytes(16);
                using (MemoryStream ms = new MemoryStream())
                {
                    using (CryptoStream cs = new CryptoStream(ms, encryptor.CreateEncryptor(), CryptoStreamMode.Write))
                    {
                        cs.Write(clearBytes, 0, clearBytes.Length);
                        cs.Close();
                    }
                    clearText = Convert.ToBase64String(ms.ToArray());
                }
            }
            return clearText;
        }

        public static string Decrypt(string cipherText, string encryptionKey)
        {
            cipherText = cipherText.Replace(" ", "+");
            byte[] cipherBytes = Convert.FromBase64String(cipherText);
            using (Aes encryptor = Aes.Create())
            {
                Rfc2898DeriveBytes pdb = new Rfc2898DeriveBytes(encryptionKey, new byte[] { 0x49, 0x76, 0x61, 0x6e, 0x20, 0x4d, 0x65, 0x64, 0x76, 0x65, 0x64, 0x65, 0x76 });
                encryptor.Key = pdb.GetBytes(32);
                encryptor.IV = pdb.GetBytes(16);
                using (MemoryStream ms = new MemoryStream())
                {
                    using (CryptoStream cs = new CryptoStream(ms, encryptor.CreateDecryptor(), CryptoStreamMode.Write))
                    {
                        cs.Write(cipherBytes, 0, cipherBytes.Length);
                        cs.Close();
                    }
                    cipherText = Encoding.Unicode.GetString(ms.ToArray());
                }
            }
            return cipherText;
        }

        /// <summary>
        /// Fixes bad html characters and replaces them with proper entity (&amp;ldquo; &amp;rdquo, ..)
        /// </summary>
        /// <param name="html"></param>
        /// <returns></returns>
        static string FixEntities(string html)
        {
            NameValueCollection nvc = new NameValueCollection();
            nvc.Add("“", "&ldquo;");
            nvc.Add("”", "&rdquo;");
            nvc.Add("–", "&mdash;");
            foreach (string key in nvc.Keys)
            {
                html = html.Replace(key, nvc[key]);
            }
            return html;
        }

        /// <summary>
        /// Makes sure that character is surrounded with white space. For example: if we have word&amp;word this can't wrap so we surround character &amp; with white space on both sides.
        /// </summary>
        /// <param name="text"></param>
        /// <param name="c"></param>
        /// <returns></returns>
        public static string ForceWrap(string text, char c)
        {
            int pos = text.IndexOf(c);
            int offset = 0;
            while (pos >= 0)
            {
                if (pos > 0 && text[pos - 1] != ' ') { text = text.Insert(pos, " "); pos++; }
                if (pos < text.Length - 1 && text[pos + 1] != ' ') { text = text.Insert(pos + 1, " "); pos++; }
                offset = pos + 1;
                pos = text.IndexOf(c, offset);
            }
            return text;
        }

        /// <summary>Converts any whitespace to single regular spaces, removes leading and trailing spaces</summary>
        /// <param name="s"></param>
        /// <returns></returns>
        public static string CleanWhitespaces(string s)
        {
            char[] whitespaces = {
                (char)9, (char)10, (char)11, (char)12, (char)13,
                (char)160, (char)5760, (char)6158, (char)8194, (char)8195, (char)8196, (char)8197,
                (char)8198, (char)8199, (char)8200, (char)8201, (char)8202, (char)8203, (char)8204,
                (char)8205, (char)8239, (char)8287, (char)8288, (char)12288, (char)65279 };
            for (int x = 0; x < whitespaces.Length; x++) s = s.Replace(whitespaces[x], ' ');
            for (s = s.Trim(); s.Contains("  "); s = s.Replace("  ", " ")) ; //Remove all extra spaces
            return s;
        }

        /// <summary>
        /// Makes sure nonbreaking (no wrap) characters are surrounded with white space. Currently supported: &amp; and /.
        /// </summary>
        /// <param name="text"></param>
        /// <returns></returns>
        public static string ForceWrap(string text)
        {
            text = ForceWrap(text, '/');
            text = ForceWrap(text, '&');
            return text;
        }

        public static string RenderPartialViewToString(string viewToRender, ViewDataDictionary viewData, ControllerContext controllerContext)
        {
            var result = ViewEngines.Engines.FindView(controllerContext, viewToRender, null);

            StringWriter output;
            using (output = new StringWriter())
            {
                var viewContext = new ViewContext(controllerContext, result.View, viewData, controllerContext.Controller.TempData, output);
                result.View.Render(viewContext, output);
                result.ViewEngine.ReleaseView(controllerContext, result.View);
            }

            return output.ToString();
        }

        public static string RootUrl()
        {
            //return HttpContext.Current.Request.Url.AbsoluteUri.Replace(HttpContext.Current.Request.Url.PathAndQuery, "/");
            return RootUrl(false);
        }

        public static string RootUrl(bool noLastSlash)
        {
            return HttpContext.Current.Request.Url.AbsoluteUri.Replace(HttpContext.Current.Request.Url.PathAndQuery, (noLastSlash ? string.Empty : "/"));
        }

        public static string RootUrl(this HttpRequestBase request)
        {
            //var uri = request?.UrlReferrer;
            //if (uri == null)
            //    return string.Empty;
            //return uri.Scheme + Uri.SchemeDelimiter + uri.Authority;
            return HttpContext.Current.Request.Url.AbsoluteUri.Replace(HttpContext.Current.Request.Url.PathAndQuery, "/");
        }


        //public static string RootUrl()
        //{
        //    return HttpContext.Current.Request.Url.AbsoluteUri.Replace(HttpContext.Current.Request.Url.PathAndQuery, "/");
        //    //return HttpContext.Current.Request.Url.Scheme + System.Uri.SchemeDelimiter + HttpContext.Current.Request.Url.Host;
        //}

        public static string ImageToBase64(Image image)
        {
            using (MemoryStream ms = new MemoryStream())
            {
                image.Save(ms, image.RawFormat);
                return Convert.ToBase64String(ms.ToArray());
            }
        }

        public static Image Base64ToImage(string base64String)
        {
            byte[] imageBytes = Convert.FromBase64String(base64String);
            MemoryStream ms = new MemoryStream(imageBytes, 0, imageBytes.Length);

            ms.Write(imageBytes, 0, imageBytes.Length);
            Image image = Image.FromStream(ms, true);
            return image;
        }

        public static string FixFolderName(string path)
        {
            path = path.Replace('/', '\\');
            if (path.Length >= 2)
            {
                if (path.Substring(0, 2) != "\\\\")
                {
                    while (path.Contains("\\\\"))
                        path = path.Replace("\\\\", "\\");
                }
            }
            return path;
        }

        public static Image DownloadImageFromUrl(string imageUrl)
        {
            Image image = null;

            try
            {
                System.Net.ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12 | SecurityProtocolType.Tls11 | SecurityProtocolType.Tls;
                HttpWebRequest webRequest = (HttpWebRequest)HttpWebRequest.Create(imageUrl);
                webRequest.AllowWriteStreamBuffering = true;
                webRequest.Timeout = 30000;

                WebResponse webResponse = webRequest.GetResponse();

                Stream stream = webResponse.GetResponseStream();

                image = Image.FromStream(stream);

                webResponse.Close();
            }
            catch (Exception ex)
            {
                return null;
            }

            return image;
        }

        /// <summary>
        /// Downloads a file to disk (can be binary). You need to handle all exceptions yourself.
        /// </summary>
        /// <param name="url"></param>
        /// <param name="outputFile"></param>
        public static void Download(string url, string outputFile)
        {
            Download(url, outputFile, null, true, false);
        }
        public static void Download(string url, string outputFile, bool overwrite)
        {
            Download(url, outputFile, null, true, overwrite);
        }
        public static void Download(string url, string outputFile, NetworkCredential credentials)
        {
            Download(url, outputFile, credentials, true, false);
        }
        public static void Download(string url, string outputFile, NetworkCredential credentials, bool overwrite)
        {
            Download(url, outputFile, credentials, false, overwrite);
        }
        public static void Download(string url, string outputFile, NetworkCredential credentials, bool usePassive, bool overwrite)
        {
            FileMode mode = FileMode.CreateNew;
            if (overwrite) mode = FileMode.Create;

            WebRequest wr;
            FtpWebRequest fr;
            WebRequest objRequest;

            if (url.Contains("ftp://"))
            {
                fr = (FtpWebRequest)FtpWebRequest.Create(new Uri(url));
                if (credentials != null) fr.Credentials = credentials;
                fr.Proxy = null;
                fr.UsePassive = usePassive;
                fr.UseBinary = true;
                objRequest = (WebRequest)fr;
            }
            else
            {
                wr = (HttpWebRequest)WebRequest.Create(new Uri(url));
                wr.Proxy = null;
                if (credentials != null) wr.Credentials = credentials;
                objRequest = (WebRequest)wr;
            }

            WebResponse objResponse = objRequest.GetResponse();
            byte[] buffer = new byte[32768];
            using (Stream input = objResponse.GetResponseStream())
            {
                using (FileStream output = new FileStream(outputFile, mode))
                {
                    int bytesRead;
                    while ((bytesRead = input.Read(buffer, 0, buffer.Length)) > 0)
                    {
                        output.Write(buffer, 0, bytesRead);
                    }
                    output.Close();
                }
                input.Close();
            }
            objResponse.Close();
        }

        public static bool FTPDownload(string url, string outputFile, string userName, string password, ref string err)
        {
            err = string.Empty;

            try
            {
                DateTime startTime = DateTime.UtcNow;
                WebRequest request = WebRequest.Create(url);
                if (userName.Length > 0 && password.Length > 0)
                {
                    request.Credentials = new System.Net.NetworkCredential(userName, password);
                }
                WebResponse response = request.GetResponse();
                using (Stream responseStream = response.GetResponseStream())
                {
                    using (Stream fileStream = System.IO.File.OpenWrite(outputFile))
                    {
                        byte[] buffer = new byte[4096];
                        int bytesRead = responseStream.Read(buffer, 0, 4096);
                        while (bytesRead > 0)
                        {
                            fileStream.Write(buffer, 0, bytesRead);
                            DateTime nowTime = DateTime.UtcNow;
                            if ((nowTime - startTime).TotalMinutes > 5)
                            {
                                return false;
                            }
                            bytesRead = responseStream.Read(buffer, 0, 4096);
                        }
                    }
                }
                return true;
            }
            catch (Exception ex)
            {
                err = ex.ToString();
                return false;
            }
        }

        public static T GetEnumAttribute<T>(Enum enumitem) where T : System.Attribute
        {
            var type = enumitem.GetType();
            var memInfo = type.GetMember(enumitem.ToString());

            var attributes = memInfo[0].GetCustomAttributes(typeof(T), false);

            return (attributes.Length > 0) ? (T)attributes[0] : null;
        }

        public static string GeneratePictureTag(string url, string variation, string alt = "", string title = "", string cls = "", int maxWidth = 0, bool displayNone = false)
        {
            string pictureTag = string.Empty;
            if (url.Length > 0)
            {
                pictureTag += "<picture" + (displayNone ? " style=\"display:none;\"" : string.Empty) + ">\n";
                pictureTag += "	<source media=\"(max-width: 320px)\" srcset=\"" + HttpUtility.UrlPathEncode("/imagelib/" + (maxWidth > 0 && maxWidth < 640 ? maxWidth : 640) + "-" + variation + url) + "\" >\n"; // 640
                pictureTag += "	<source media=\"(max-width: 640px)\" srcset=\"" + HttpUtility.UrlPathEncode("/imagelib/" + (maxWidth > 0 && maxWidth < 960 ? maxWidth : 960) + "-" + variation + url) + "\" >\n"; // 960
                pictureTag += "	<source media=\"(max-width: 960px)\" srcset=\"" + HttpUtility.UrlPathEncode("/imagelib/" + (maxWidth > 0 && maxWidth < 1360 ? maxWidth : 1360) + "-" + variation + url) + "\" >\n"; // 1360
                pictureTag += "	<source media=\"(max-width: 1360px)\" srcset =\"" + HttpUtility.UrlPathEncode("/imagelib/" + (maxWidth > 0 && maxWidth < 1840 ? maxWidth : 1840) + "-" + variation + url) + "\" >\n"; // 1840
                pictureTag += "	<source media=\"(max-width: 1840px)\" srcset =\"" + HttpUtility.UrlPathEncode("/imagelib/" + (maxWidth > 0 && maxWidth < 1920 ? maxWidth : 1920) + "-" + variation + url) + "\" >\n"; // 1920
                pictureTag += "	<source media=\"(min-width: 1841px)\" srcset =\"" + HttpUtility.UrlPathEncode("/imagelib/" + (maxWidth > 0 && maxWidth < 2560 ? maxWidth : 2560) + "-" + variation + url) + "\" >\n"; // 2560
                pictureTag += "	<img data-src=\"" + HttpUtility.UrlPathEncode("/imagelib/" + (maxWidth > 0 && maxWidth < 640 ? maxWidth : 640) + "-" + variation + url) + "\" alt =\"" + HttpUtility.HtmlEncode(alt) + "\" title=\"" + HttpUtility.HtmlEncode(title) + "\" " + (cls.Length > 0 ? "class=\"" + cls + "\"" : string.Empty) + ">\n";
                pictureTag += "</picture>\n";
            }

            return pictureTag;
        }

    }
}
