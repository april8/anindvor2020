﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Hangfire;
using Owin;

namespace A8
{
    public static class HangfireJobs
    {
        public static void InitJobs(IAppBuilder app)
        {
            /*
            Hangfire
            https://www.hangfire.io/

            Note:
            Scheduler works only when app_pool is up. When app_pool is recycled or stoped, or server is down, scheduler does not work.
            When app_pool is back and at least one client call is made (anywhere on site, including api calls), scheduler (aka this class) is instantiated.
            For this purpose, at hosting environment, create scheduled task to periodicaly call some dummy api to keep app waked-up.
            Note also, that any change on app code, will also reinstantiate this class.

            Add singular (fire-and-forget) job anywhere in the project via
                 using Hangfire;
                 var jobId1 = BackgroundJob.Schedule(() => UniqueNamedFunction(), TimeSpan.FromMinutes(5));
                (be careful to name funcitons with unique name, while they are parsed as json into Hangfire schedule)

            You can define retries with decoration before function. For simple jobs dissable automatic retries
                 [Hangfire.AutomaticRetry(Attempts = 3, Intervals = "1200")] // Intervals in seconds separated by commas
                 public void UniqueNamedFunction() { ... }

            Put recurring jobs here in this class
                RecurringJob.AddOrUpdate(() => UniqueNamedFunction(), "0 1 * * *"); // every day at 1:00 oclock
                (you can use cron 5-digits expressions, see cron tester: https://crontab.guru/)

            Note:
                When creatig recurring jobs, be sure to implement non-reentrant method/function (see idempotency) to avoid double/overlaping actions.
                For example: when sending mass mailing, create custom table and flag each sent mail.
                In case scheduler is re-instantiated - those mails will not be send again.
                Hangfire writes log in db. Sucedeed jobs are automatically deleted from db, while failed remained. Check db over time, if needs to be cleaned.
             */

            //Samples
            //var testJob = BackgroundJob.Schedule(() => TestJob(), TimeSpan.FromMinutes(1)); // fire everytime when app is modified

            //var jobId2 = BackgroundJob.Schedule(() => A8.fn.DoNothing2(), TimeSpan.FromMinutes(5));

            //RecurringJob.AddOrUpdate(() => A8.fn.DoNothing1(), "*/1 * * * *"); // every minute
            //RecurringJob.AddOrUpdate(() => A8.fn.DoNothing2(), "*/5 * * * *"); // every 5-minutes

            //RecurringJob.AddOrUpdate(() => A8.fn.DoNothing1(), Cron.Minutely);
            //RecurringJob.AddOrUpdate(() => A8.fn.DoNothing2(), Cron.Minutely);

            //RecurringJob.AddOrUpdate(() => A8.LayoutFunctions.UnpublishPassedEvents(), "0 1 * * *");
        }

        public static void TestJob()
        {
            A8.Common.SendToDeveloper("Firekit test scheduler (re)initialized", "");
        }
    }
}