﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Configuration;
using System.Net.Mail;
using System.Web;

namespace A8
{
    public class Email
    {
        public class EmbeddedObject
        {
            string filename;
            string name;
            string mimeType;

            public EmbeddedObject(string filename, string name, string mimeType)
            {
                this.filename = filename;
                this.name = name;
                this.mimeType = mimeType;
            }

            public string Filename
            {
                get { return this.filename; }
            }

            public string Name
            {
                get { return this.name; }
            }

            public string MimeType
            {
                get { return this.mimeType; }
            }
        }

        static LinkedResource CreateEmbeddedObject(string filename, string referenceName, string mimeType)
        {
            // FileStream fs = File.OpenRead(filename);
            // LinkedResource data = new LinkedResource((Stream)fs);
            LinkedResource data = new LinkedResource(filename);

            data.ContentType.MediaType = mimeType;

            data.ContentType.Name = referenceName;
            data.ContentId = referenceName;
            data.ContentLink = new Uri("cid:" + referenceName);

            return data;
        }

        /// <summary>
        /// Returns list of recipients. Separator can be , or ;
        /// </summary>
        /// <param name="recipients"></param>
        /// <returns></returns>
        public static List<MailAddress> CreateRecipients(string recipients)
        {
            string rr = A8.Common.CheckStr(recipients).Replace(",", ";");
            string[] tmp = rr.Split(';');
            if (tmp != null && tmp.Length > 0)
            {
                List<MailAddress> r = new List<MailAddress>();
                for (int x = 0; x <= tmp.Length - 1; x++)
                {
                    tmp[x] = tmp[x].Trim();
                    if (A8.Common.VerifyEmail(tmp[x])) r.Add(new MailAddress(tmp[x]));
                }

                if (r.Count > 0) return r;
            }
            return null;
        }

        public static List<string> RecipientsToString(List<MailAddress> recipients)
        {
            if (recipients != null)
            {
                List<string> recs = new List<string>();
                for (int x = 0; x < recipients.Count; x++)
                    recs.Add(recipients[x].Address);

                return recs;
            }

            return null;
        }

        public static void SendHtmlEmail(MailAddress sender, List<MailAddress> recipients, List<MailAddress> bccRecipients, string subject, string body)
        {
            SendHtmlEmail(sender, recipients, bccRecipients, subject, body, null, null, null);
        }

        /// <summary>
        /// Sends UTF8 html email
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="recipients"></param>
        /// <param name="bccRecipients"></param>
        /// <param name="subject"></param>
        /// <param name="body"></param>
        /// <param name="embeddedObjects">embedded object can be referenced as cid:reference_code</param>
        /// <param name="attachments">list of absolute paths to attachment file. Name of the attachment can be separated with ; - c:\some\file.pdf;friendly file.pdf</param>
        public static void SendHtmlEmail(MailAddress sender, List<MailAddress> recipients, List<MailAddress> bccRecipients, string subject, string body, List<Email.EmbeddedObject> embeddedObjects, List<string> attachments)
        {
            SendHtmlEmail(sender, recipients, bccRecipients, subject, body, embeddedObjects, attachments, null, null, null);
        }

        public static void SendHtmlEmail(MailAddress sender, List<MailAddress> recipients, List<MailAddress> bccRecipients, string subject, string body, List<Email.EmbeddedObject> embeddedObjects, List<string> attachments, Attachment attachment)
        {
            SendHtmlEmail(sender, recipients, bccRecipients, subject, body, embeddedObjects, attachments, attachment, null, null);
        }

        public static void SendHtmlEmail(MailAddress sender, List<MailAddress> recipients, List<MailAddress> bccRecipients, string subject, string body, List<Email.EmbeddedObject> embeddedObjects, List<string> attachments, Attachment streamAttachment, MailAddress onBehalf = null, MailAddressCollection replyTo = null)
        {
            int x;

            if (recipients == null || recipients.Count <= 0) return;

            if (body.IndexOf("<html", StringComparison.OrdinalIgnoreCase) < 0)
            {
                body = "<html>" +
                       "<head><meta content=\"text/html; charset=utf-8\" http-equiv=\"content-type\" /></head>" +
                       "<body>" +
                       body +
                       "</body>" +
                       "</html>";
            }

            byte[] bytes = System.Text.Encoding.UTF8.GetBytes(body);

            MemoryStream hs = new MemoryStream();
            hs.Write(bytes, 0, bytes.Length);
            hs.Position = 0;

            // AlternateView altView = new AlternateView(hs, MediaTypeNames.Text.Html);
            AlternateView altView = AlternateView.CreateAlternateViewFromString(body, System.Text.Encoding.UTF8, "text/html");

            LinkedResource res;
            if (embeddedObjects != null)
            {
                for (x = 0; x <= embeddedObjects.Count - 1; x++)
                {
                    if ((res = CreateEmbeddedObject(embeddedObjects[x].Filename, embeddedObjects[x].Name, embeddedObjects[x].MimeType)) != null)
                        altView.LinkedResources.Add(res);
                }
            }

            MailMessage mailMessage = new MailMessage();
            mailMessage.Body = "";
            mailMessage.AlternateViews.Add(altView);
            mailMessage.From = sender;

            if (onBehalf != null)
            {
                mailMessage.Sender = sender;
                mailMessage.From = onBehalf;
            }

            if (replyTo != null)
            {
                foreach (MailAddress addr in replyTo)
                    mailMessage.ReplyToList.Add(addr);
            }


            for (int r = 0; r <= recipients.Count - 1; r++) mailMessage.To.Add(recipients[r]);
            if (bccRecipients != null && bccRecipients.Count > 0)
                for (int r = 0; r <= bccRecipients.Count - 1; r++) mailMessage.Bcc.Add(bccRecipients[r]);

            mailMessage.SubjectEncoding = System.Text.Encoding.UTF8;
            mailMessage.Subject = subject;
            mailMessage.Priority = MailPriority.Normal;
            mailMessage.IsBodyHtml = true;
            mailMessage.BodyEncoding = System.Text.Encoding.UTF8;


            List<FileStream> attachmentStreams = new List<FileStream>();
            if (attachments != null)
            {
                for (x = 0; x <= attachments.Count - 1; x++)
                {
                    string file = attachments[x];
                    string[] parts = file.Split(';');
                    string name = file;
                    if (parts.Length > 1)
                    {
                        file = parts[0].Trim();
                        name = parts[1].Trim();
                    }
                    FileStream fs = new FileStream(file, FileMode.Open);
                    mailMessage.Attachments.Add(new Attachment(fs, name));
                    attachmentStreams.Add(fs);
                }
            }

            if (streamAttachment != null)
            {
                mailMessage.Attachments.Add(streamAttachment);
            }

            SmtpClient client = new SmtpClient();
            SmtpSection section = (SmtpSection)ConfigurationManager.GetSection("system.net/mailSettings/smtp");

            if (section != null)
            {
                client.DeliveryMethod = section.DeliveryMethod;
                client.Host = A8.Common.CheckStr(section.Network.Host);
                client.Port = A8.Common.CheckNum(section.Network.Port, 25);
                if (A8.Common.VerifyTrue(section.Network.EnableSsl))
                {
                    client.EnableSsl = true;
                }

                if (A8.Common.CheckStr(section.Network.UserName).Length > 0 && A8.Common.CheckStr(section.Network.Password).Length > 0)
                {
                    NetworkCredential basicCredential =
                        new NetworkCredential(A8.Common.CheckStr(A8.Common.CheckStr(section.Network.UserName)), A8.Common.CheckStr(A8.Common.CheckStr(section.Network.Password)));
                    client.UseDefaultCredentials = false;
                    client.Credentials = basicCredential;
                }
            }
            else
                client.DeliveryMethod = SmtpDeliveryMethod.PickupDirectoryFromIis;

            client.Send(mailMessage);

            for (int y = 0; y < attachmentStreams.Count; y++)
            {
                try { attachmentStreams[y].Close(); }
                catch { };
            }
        }

        /// <summary>
        /// Sends UTF8 html email
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="recipients"></param>
        /// <param name="subject"></param>
        /// <param name="body"></param>
        /// <param name="embeddedObjects">embedded object can be referenced as cid:reference_code</param>
        /// <param name="attachments">list of absolute paths to attachment file. Name of the attachment can be separated with ; - c:\some\file.pdf;friendly file.pdf</param>
        public static void SendHtmlEmail(MailAddress sender, List<MailAddress> recipients, string subject, string body, List<Email.EmbeddedObject> embeddedObjects, List<string> attachments)
        {
            SendHtmlEmail(sender, recipients, null, subject, body, embeddedObjects, attachments, null);
        }
        /// <summary>
        /// Sends UTF8 HTML email.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="recipients"></param>
        /// <param name="subject"></param>
        /// <param name="body"></param>
		public static void SendHtmlEmail(MailAddress sender, List<MailAddress> recipients, string subject, string body)
        {
            SendHtmlEmail(sender, recipients, null, subject, body, null, null, null);
        }

        /// <summary>
        /// Sends UTF8 html email
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="recipient"></param>
        /// <param name="subject"></param>
        /// <param name="body"></param>
        /// <param name="embeddedObjects">embedded object can be referenced as cid:reference_code</param>
        /// <param name="attachments">list of absolute paths to attachment file. Name of the attachment can be separated with ; - c:\some\file.pdf;friendly file.pdf</param>
        public static void SendHtmlEmail(MailAddress sender, MailAddress recipient, string subject, string body, List<Email.EmbeddedObject> embeddedObjects, List<string> attachments)
        {
            List<MailAddress> r = new List<MailAddress>();
            r.Add(recipient);
            SendHtmlEmail(sender, r, null, subject, body, embeddedObjects, attachments);
        }
        public static void SendHtmlEmail(MailAddress sender, MailAddress recipient, string subject, string body)
        {
            SendHtmlEmail(sender, recipient, subject, body, null, null);
        }
    }
}