﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Umbraco.Core;
using Umbraco.Core.Services;
using Umbraco.Web.Trees;
using DictionaryHelper;
using Umbraco.Core.Persistence;
using Umbraco.Web.Editors;
using Umbraco.Web.Models.ContentEditing;
using System.Web.Optimization;
using JavaScriptEngineSwitcher.Core;

public class GlobalStartup : ApplicationEventHandler
{
    protected override void ApplicationStarted(UmbracoApplicationBase umbracoApplication, ApplicationContext applicationContext)
    {
        A8.JsEngineSwitcherConfig.Configure(JsEngineSwitcher.Current);
        A8.Core.BundleConfig.RegisterBundles(BundleTable.Bundles);

        ContentService.Published += A8.Cache.PageCache.ContentService_Published;
        ContentService.UnPublished += A8.Cache.PageCache.ContentService_UnPublished;

        // Menu Events (Show/Hide, ..)
        TreeControllerBase.MenuRendering += A8.UmbracoMenuContentTree.ContentTreeController_MenuRendering;
        TreeControllerBase.TreeNodesRendering += A8.UmbracoMenuContentTree.ContentTreeController_Style;

        A8.Core.FormEditor.SqlIndex.Storage.Index.EnsureDatabase(applicationContext);

        //Dictionary translation init
        LocalizationService.SavedDictionaryItem += DictionaryHelper.Startup.SavedDictionaryItem;
        LocalizationService.DeletingDictionaryItem += DictionaryHelper.Startup.DeletingDictionaryItem;
        DictionaryCache.Fill();
    }
}
