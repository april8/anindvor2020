﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Web.Helpers;

using ImageProcessor;
using ImageProcessor.Imaging;

using Umbraco.Core;
using Umbraco.Core.Configuration;
using Umbraco.Core.Configuration.UmbracoSettings;
using Umbraco.Core.IO;
using Umbraco.Core.Logging;
using Umbraco.Core.Models;
using Umbraco.Core.Services;

namespace A8
{
    /// <summary>
    /// Attaches ImageProcessor to the Media.Saving event to ensure that uploaded files do not exceed
    /// a maximum size.
    /// </summary>
    public class ImageUploadEventHandler : ApplicationEventHandler
    {
        protected override void ApplicationStarting(UmbracoApplicationBase umbracoApplication, ApplicationContext applicationContext)
        {
            const string UmbracoFile = Umbraco.Core.Constants.Conventions.Media.File;

            // Tap into the Saved event
            MediaService.Saved += (sender, args) =>
            {
                MediaFileSystem mediaFileSystem = FileSystemProviderManager.Current.GetFileSystemProvider<MediaFileSystem>();
                IContentSection contentSection = UmbracoConfig.For.UmbracoSettings().Content;
                IEnumerable<string> supportedTypes = contentSection.ImageFileTypes;

                foreach (IMedia media in args.SavedEntities)
                {
                    if (media.HasProperty(UmbracoFile))
                    {
                        string path = media.GetValue<string>(UmbracoFile);

                        try
                        {
                            // Make sure it's not empty.
                            if (string.IsNullOrWhiteSpace(path))
                            {
                                return;
                            }

                            // This might happen after changing the data type of umbracoFile from 
                            // Image Cropper back to Upload. 
                            if (path.Contains("{"))
                            {
                                dynamic cropper = Json.Decode(path);
                                string src = cropper.Src;
                                if (!string.IsNullOrWhiteSpace(src))
                                {
                                    path = src;
                                }
                                else
                                {
                                    return;
                                }
                            }

                            // Make sure it's an image.
                            string extension = Path.GetExtension(path).Substring(1);
                            if (supportedTypes.InvariantContains(extension))
                            {
                                // Resize the image, height is driven by the
                                // aspect ratio of the image.
                                string fullPath = mediaFileSystem.GetFullPath(path);
                                using (ImageFactory imageFactory = new ImageFactory(true))
                                {
                                    // Hardcoded at 2560 but could be configurable.
                                    ResizeLayer layer = new ResizeLayer(new Size(2560, 0), ResizeMode.Max)
                                    {
                                        Upscale = false
                                    };

                                    // Resize
                                    imageFactory.Load(fullPath).Resize(layer);

                                    int newWidth = imageFactory.Image.Width;
                                    int newHeight = imageFactory.Image.Height;

                                    // Save
                                    imageFactory.Save(fullPath);

                                    FileInfo info = new FileInfo(fullPath);
                                    long bytes = info.Length;

                                    // Set the correct meta values for the image.
                                    media.SetValue(Umbraco.Core.Constants.Conventions.Media.Width, newWidth);
                                    media.SetValue(Umbraco.Core.Constants.Conventions.Media.Height, newHeight);
                                    media.SetValue(Umbraco.Core.Constants.Conventions.Media.Bytes, bytes);
                                    sender.Save(media, 0, false);
                                }
                            }
                        }
                        catch (Exception ex)
                        {
                            // For all unexpected errors we get a hint in the log file.
                            LogHelper.Error<ImageUploadEventHandler>(
                                string.Format("Could not resize image. Path: {0}", path), ex);
                        }
                    }
                }
            };
        }
    }
}