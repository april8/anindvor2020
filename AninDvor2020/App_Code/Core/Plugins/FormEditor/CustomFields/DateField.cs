﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using FormEditor.Fields;
using Umbraco.Core.Models;

namespace A8.FormEditor.Fields
{
	public class DateField : FieldWithMandatoryValidation
	{
		public DateField()
		{
		}

		public override string Type => "a8.custom.date";
		public override string PrettyName => "Date (A8)";

		public string UrlParameter {
			get;
			set;
		}
			
		public string Value {
			get
			{
				if (!String.IsNullOrEmpty(UrlParameter) && !String.IsNullOrEmpty(HttpContext.Current.Request.QueryString[UrlParameter]) && this.HasSubmittedValue == false)
				{
					DateTime date = DateTime.MinValue;
					DateTime.TryParseExact(HttpContext.Current.Request.QueryString[UrlParameter], "d.M.yyyy", null, System.Globalization.DateTimeStyles.None, out date);
					this.SubmittedValue = date.ToString("dd. MM. yyyy");
				}
				return this.SubmittedValue;
			}
			set {
			}
		}
	}
}