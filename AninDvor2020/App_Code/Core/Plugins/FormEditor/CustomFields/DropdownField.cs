﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Web;
using System.Web.WebPages;
using FormEditor.Fields;
using FormEditor.Fields.Statistics;
using Newtonsoft.Json;
using Umbraco.Core.Models;

namespace A8.FormEditor.Fields
{
	public class DropdownField : A8.FormEditor.Fields.FieldWithFieldValues
	{
		public DropdownField()
		{
			FieldValue fv1 = new A8.FormEditor.Fields.FieldValue { Value = "value_1" };
			fv1.Text = "Value 1";
			FieldValue fv2 = new A8.FormEditor.Fields.FieldValue { Value = "value_2" };
			fv2.Text = "Value 2";
			fv2.Selected = true;
			FieldValues = new[] { fv1, fv2 };
			
			DefaultText = "Select... ";
		}

		public override string Type => "a8.custom.dropdown";
		public override string PrettyName => "Select field (A8)";

		public string UrlParameter
		{
			get;
			set;
		}
		
		public string DefaultText { get; set; } 

		public HelperResult GetPreselectedValue()
		{
			if (!String.IsNullOrEmpty(HttpContext.Current.Request.QueryString[UrlParameter]) && this.HasSubmittedValue == false)
			{
				string selectedFromParam = A8.Common.CheckStr(HttpContext.Current.Request.QueryString[UrlParameter]);
				if (selectedFromParam != "")
				{
					if (this.FieldValues.FirstOrDefault(item => item.Value == selectedFromParam) != null)
					{
						this.FieldValues.ToList().ForEach(item => item.Selected = false);
						this.FieldValues.FirstOrDefault(item => item.Value == selectedFromParam).Selected = true;
					}
				}
			}
			return null;
		}
	}

	public abstract class FieldWithFieldValues : FieldWithMandatoryValidation, IValueFrequencyStatisticsField, IStatisticsField
	{
		public A8.FormEditor.Fields.FieldValue[] FieldValues { get; set; }
		public virtual bool IsMultiSelectEnabled { get; }
		[JsonIgnore]
		public IEnumerable<string> SubmittedValues { get; }
		public virtual bool MultipleValuesPerEntry { get; }

		protected override void CollectSubmittedValue(Dictionary<string, string> allSubmittedValues, IPublishedContent content)
		{
			base.CollectSubmittedValue(allSubmittedValues, content);

			if (string.IsNullOrEmpty(SubmittedValue))
			{
				return;
			}
			if (SubmittedValue.StartsWith("[\"") && SubmittedValue.EndsWith("\"]"))
			{
				// #168: if the submitted value is a JSON string array, parse it to the expected CSV format 
				SubmittedValue = string.Join(",", JsonConvert.DeserializeObject<string[]>(SubmittedValue));
			}
		}

		protected override bool ValidateSubmittedValue(IEnumerable<Field> allCollectedValues, IPublishedContent content)
		{
			if (base.ValidateSubmittedValue(allCollectedValues, content) == false)
			{
				return false;
			}
			if (string.IsNullOrEmpty(SubmittedValue))
			{
				// nothing selected => valid (mandatory validation is handled by base class)
				return true;
			}

			var submittedFieldValues = ExtractSubmittedValues();
			FieldValues.ToList().ForEach(f => f.Selected = submittedFieldValues.Contains(f.Value));

			// make sure all submitted values are actually defined as a field value (maybe some schmuck tampered with the options client side)
			if (submittedFieldValues.Any())
			{
				return submittedFieldValues.All(v => FieldValues.Any(f => f.Value == v));
			}

			return true;
		}

		private string[] ExtractSubmittedValues()
		{
			return SubmittedValue != null
				? IsMultiSelectEnabled
					? SubmittedValue.Split(',')
					: new[] { SubmittedValue }
				: new string[] { };
		}

	}

	public class FieldValue
	{
		public FieldValue() { }

		public string Text { get; set; }
		public string Value { get; set; }
		public bool Selected { get; set; }
	}
}
