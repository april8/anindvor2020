﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Umbraco.Core;
using Umbraco.Core.Models;
using Umbraco.Core.Services;
using Umbraco.Web.Trees;

namespace A8
{
    public static class UmbracoMenuContentTree
    {
        public static void ContentTreeController_MenuRendering(Umbraco.Web.Trees.TreeControllerBase sender, Umbraco.Web.Trees.MenuRenderingEventArgs e)
        {
            switch (sender.TreeAlias)
            {
                case "content":
                    var umbracoHelper = new Umbraco.Web.UmbracoHelper(sender.UmbracoContext);
                    IPublishedContent content = umbracoHelper.TypedContent(A8.Common.CheckNum(e.NodeId));
                    if (content == null)
                    {
                        break;
                    }

                    IPublishedProperty item = content.GetProperty("hideInMenu");
                    if (item == null)
                    {
                        break;
                    }

                    Umbraco.Web.Models.Trees.MenuItem mi = null;
                    if (A8.Common.VerifyTrue(item.Value))
                    { // hidden, show option
                        mi = new Umbraco.Web.Models.Trees.MenuItem("show", "Show in menu");
                    }
                    else
                    { // visible, hide option
                        mi = new Umbraco.Web.Models.Trees.MenuItem("hide", "Hide in menu");
                    }

                    string javascript = "A8MenuShowHide(" + e.NodeId + ");";
                    mi.ExecuteLegacyJs(javascript);

                    mi.Icon = "eye";
                    e.Menu.Items.Insert(6, mi);
                    break;
            }
        }

        public static void ContentTreeController_Style(TreeControllerBase treeController, TreeNodesRenderingEventArgs e)
        {
            if (e.Nodes.Count <= 0)
            {
                return;
            }

            var umbracoHelper = new Umbraco.Web.UmbracoHelper(treeController.UmbracoContext);

            foreach (var node in e.Nodes)
            {
                IPublishedContent content = umbracoHelper.TypedContent(A8.Common.CheckNum(node.Id));
                if (content == null)
                {
                    continue;
                }

                IPublishedProperty item = content.GetProperty("hideInMenu");
                if (item == null)
                {
                    continue;
                }

                if (A8.Common.VerifyTrue(item.Value))
                {
                    node.CssClasses.Add("menuItemHidden");
                }
            }
        }
    }
}