﻿using ClientDependency.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http.Formatting;
using System.Web;
using System.Web.Mvc;
using System.Web.UI.WebControls;
using umbraco;
using umbraco.BusinessLogic.Actions;
using Umbraco.Web.Models.Trees;
using Umbraco.Web.Mvc;
using Umbraco.Web.Trees;

namespace A8
{
    [Tree("A8Services", "A8ServicesTree", "Services", "icon-settings", "icon-settings")]
    [PluginController("A8Services")]
    public class A8ServicesTreeController : TreeController
    {
        protected override Umbraco.Web.Models.Trees.MenuItemCollection GetMenuForNode(string id, FormDataCollection queryStrings)
        {
            Umbraco.Web.Models.Trees.MenuItemCollection menu = new Umbraco.Web.Models.Trees.MenuItemCollection();
            menu.Items.Add<RefreshNode, ActionRefresh>("Refresh");
            return menu;
        }

        protected override Umbraco.Web.Models.Trees.TreeNodeCollection GetTreeNodes(string id, FormDataCollection queryStrings)
        {
            Umbraco.Web.Models.Trees.TreeNodeCollection tree = new Umbraco.Web.Models.Trees.TreeNodeCollection();
            if (id == "-1")
            {
                Umbraco.Web.Models.Trees.MenuItem mi = new Umbraco.Web.Models.Trees.MenuItem("ClearCache", "Clear cache");
                var item = this.CreateTreeNode("ClearCache", id, queryStrings, "Clear cache", "icon-axis-rotation-2", false, "A8Services/A8ServicesTree/A8ClearCache/1");
                tree.Add(item);
            }

            return tree;
        }

    }
}
