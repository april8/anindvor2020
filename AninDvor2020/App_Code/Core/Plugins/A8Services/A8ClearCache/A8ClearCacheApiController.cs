﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web.Http;
using Umbraco.Core.Models;
using Umbraco.Web.Editors;
using Umbraco.Web.Mvc;

namespace A8
{
    [PluginController("A8ClearCache")]
    public class A8ClearCacheApiController : UmbracoAuthorizedJsonController
    {
        [HttpPost]
        public bool ClearCache()
        {
            return A8.Cache.PageCache.ClearAllCache();
        }
    }
}