﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using Umbraco.Web.WebApi;
using Umbraco.Web.Editors;
using Umbraco.Core.Persistence;

namespace A8
{
    [Umbraco.Web.Mvc.PluginController("A8")]
    public class A8DictionaryPickerMultiApiController : UmbracoAuthorizedJsonController
    {
        public IEnumerable<DictionaryKey> GetValues()
        {
            var requestedParentDictionaryKeyword = A8.Common.CheckStr(HttpContext.Current.Request.QueryString["requestedParentDictionaryKeyword"]);
            if (requestedParentDictionaryKeyword.Length > 0)
            {
                var db = UmbracoContext.Application.DatabaseContext.Database;
                //return db.Fetch<DictionaryKey>("select a.[key] as [Value] from cmsDictionary as a left outer join cmsDictionary b on a.parent = b.id where b.[key] = '" + A8.Common.StringToSql(requestedParentDictionaryKeyword) + "' order by a.[key]");
                return db.Fetch<DictionaryKey>("select a.[key] as [Key], coalesce(cast((select top 1 b.[value] from cmsLanguageText b where b.UniqueId = a.id and b.languageId = 2) as nvarchar(max)) + ' ', '') + '(' + a.[key] + ')' AS [Value] from cmsDictionary as a left outer join cmsDictionary b on a.parent = b.id where b.[key] = '" + A8.Common.StringToSql(requestedParentDictionaryKeyword) + "' order by a.[key]");
            }
            return null;
        }

        public class DictionaryKey
        {
            public string Value { get; set; }
        }
    }
}