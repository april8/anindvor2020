﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Umbraco.Core;
using Umbraco.Core.Models;
using Umbraco.Core.Models.PublishedContent;
using Umbraco.Core.Persistence;

namespace A8
{
    // for simplicity's sake all models are assembled here

    public class DateFromTo
    {
        public DateTime DateFrom { get; set; }
        public DateTime DateTo { get; set; }

        public DateFromTo() { }

        public DateFromTo(DateTime dateFrom, DateTime dateTo)
        {
            this.DateFrom = dateFrom;
            this.DateTo = dateTo;
        }
    }

    public class ContentFlowModel
    {
        public IEnumerable<IPublishedContent> ContentItems; // for embedded contents
        public IPublishedContent Content; // for document types
        public object CustomModel; // whatever
    }

    // generic model for catalogue items for lists
    public class CatalogueListItem
    {
        public int Id { get; set; }
        public string ItemType { get; set; } // for icon
        public string Image { get; set; }
        public string DatesAsString { get; set; } // for display
        public string Title { get; set; }
        public string Summary { get; set; }
        public string LinkMore { get; set; }

        public CatalogueListItem(
            int id,
            string itemType,
            string image,
            string dateAsString,
            string title,
            string summary,
            string linkMore)
        {
            this.Id = id;
            this.ItemType = itemType;
            this.Image = image;
            this.DatesAsString = dateAsString;
            this.Title = title;
            this.Summary = summary;
            this.LinkMore = linkMore;
        }
    }
}