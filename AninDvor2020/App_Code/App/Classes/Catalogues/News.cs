﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Umbraco.Core.Models;
using Umbraco.Web;
using Umbraco.Web.Models;
using Umbraco.Web.Mvc;
using Our.Umbraco.Vorto.Extensions;

namespace A8.Catalogues
{
    public class OneNews
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Image { get; set; }
        public string Type { get; set; }
        public DateTime Date { get; set; }
        public string Summary { get; set; }
        public IEnumerable<IPublishedContent> Content { get; set; }
        public string Keywords { get; set; }
        public string Description { get; set; }
        public bool ExludeFromList { get; set; }

        public OneNews(
            int id,
            string name,
            string image,
            string type,
            DateTime date,
            string summary,
            IEnumerable<IPublishedContent> content,
            string keywords,
            string description,
            bool excludeFromList
            )
        {
            this.Id = id;
            this.Name = name;
            this.Image = image;
            this.Type = type;
            this.Date = date;
            this.Summary = summary;
            this.Content = content;
            this.Keywords = keywords;
            this.Description = description;
            this.ExludeFromList = excludeFromList;
        }
    }

    public static class News
    {
        /// <summary>
        /// Return items as IpublishContent
        /// </summary>
        public static List<IPublishedContent> LoadAllINews(NewsFilter filter)
        {
            return _LoadAllNews(filter);
        }

        /// <summary>
        /// Return items as News
        /// </summary>
        public static List<OneNews> LoadAllNews(NewsFilter filter)
        {
            return LoadNews(_LoadAllNews(filter), filter);
        }

        private static List<IPublishedContent> _LoadAllNews(NewsFilter filter)
        {
            List<IPublishedContent> lstItems = new List<IPublishedContent>();

            int rootId = Array.ConvertAll(A8.CommonUmbraco.GetCurrentContent().Path.Split(','), int.Parse)[1];
            IEnumerable<IPublishedContent> items = UmbracoContext.Current.ContentCache.GetByXPath("id(" + rootId + ")[@isDoc]/ancestor-or-self::site[@isDoc]/cat_Catalogues[@isDoc]/cat_News[@isDoc]/cat_OneNews[@isDoc]").OfType<IPublishedContent>().Where(x => x.IsVisible())?.OrderByDescending(x => (DateTime)x.GetPropertyValue<DateTime>("date"))?.ToList();

            if (items.Any() && filter != null && filter.OneNewsType != null && filter.OneNewsType.Length > 0)
            {
                items = items.Where(x => ((string)x.GetPropertyValue<string>("catalogueType") == filter.OneNewsType)).ToList();
            }

            if (items.Any() && filter != null && filter.OneNewsType != null && filter.LastN > 0)
            {
                items = items.Take(filter.LastN);
            }

            //if (items.Any() && filter != null && filter.OneNewsType != null && filter.OneNewsType.Any())
            //{
            //    items = items.Where(x => ((string[])x.GetPropertyValue<string>("catalogueType").Split(',')).Intersect(filter.OneNewsType).Any());
            //}

            if (items.Any())
            {
                lstItems = items.ToList();
            }

            return lstItems;
        }

        public static OneNews LoadOneNews(IPublishedContent content)
        {
            List<IPublishedContent> contents = new List<IPublishedContent>();
            if (content != null)
            {
                contents.Add(content);
            }
            return LoadNews(contents, null).FirstOrDefault();
        }

        public static List<OneNews> LoadPickerNews(IEnumerable<IPublishedContent> items)
        {
            List<IPublishedContent> lstItems = new List<IPublishedContent>();

            if (items != null && items.Any())
            {
                lstItems = items.ToList();
            }

            return LoadNews(lstItems, null);
        }

        public static List<OneNews> LoadNews(List<IPublishedContent> contents, NewsFilter filter)
        {
            List<OneNews> items = new List<OneNews>();
            if (contents != null && contents.Any())
            {
                foreach (var item in contents)
                {
                    //bool includeInList = true;
                    int id = item.Id;
                    string name = string.Empty;

                    string image = string.Empty;
                    var oImage = item.GetPropertyValue<IPublishedContent>("image");
                    if (oImage != null)
                    {
                        image = oImage.Url;
                    }

                    // date is mandatory
                    DateTime date = item.GetPropertyValue<DateTime>("date");
                    if (date == null && date == DateTime.MinValue)
                    {
                        continue;

                        //date = DateTime.MinValue;
                        //includeInList = false;
                    }

                    string type = item.GetPropertyValue<string>("type");
                    string summary = string.Empty;
                    IEnumerable<IPublishedContent> content = null;
                    string keywords = string.Empty;
                    string description = string.Empty;

                    bool excludeFromList = false;
                    var langsContent = item.GetVortoValue<IPublishedContent>("langs");
                    if (langsContent != null)
                    {
                        excludeFromList = langsContent.GetPropertyValue<bool>("langsNewsExcludeFromList");
                        if (filter != null && excludeFromList && filter.ApplyExcludedFromList)
                        {
                            continue;

                            //includeInList = false;
                        }

                        name = langsContent.GetPropertyValue<string>("langsNewsTitle");
                        summary = langsContent.GetPropertyValue<string>("langsNewsSummary");
                        content = langsContent.GetPropertyValue<IEnumerable<IPublishedContent>>("langsNewsContent");
                        IPublishedContent oSeo = langsContent.GetPropertyValue<IPublishedContent>("langsNewsSeo");
                        keywords = oSeo.GetPropertyValue<string>("seoKeywords");
                        description = oSeo.GetPropertyValue<string>("seoDescription");
                    }
                    else
                    {
                        continue;

                        //includeInList = false;
                    }
                    if (name.Length == 0) name = item.Name;

                    //if (includeInList)
                    {
                        items.Add(new A8.Catalogues.OneNews(
                            id,
                            name,
                            image,
                            type,
                            date,
                            summary,
                            content,
                            keywords,
                            description,
                            excludeFromList
                        ));
                    }
                }
            }

            // finally sort by date desc
            // return items?.OrderByDescending(x => x.Date).ToList();

            // order-by moved to filter
            return items;
        }
    }

    public class NewsFilter
    {
        public string OneNewsType { get; set; }
        public bool ApplyExcludedFromList { get; set; }
        public int LastN { get; set; }

        public NewsFilter() { }
    }
}