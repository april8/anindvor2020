﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace A8
{
    public class NoPreviewController: Controller
    {
        public ActionResult Index()
        {
            return Content("This page intentionally has no visible content.");
        }
    }
}