﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web.Mvc;
using Umbraco.Core.Models;
using Umbraco.Web;
using Umbraco.Web.Mvc;
using Umbraco.Web.WebApi;

namespace A8
{
    public class CookieConsentController : SurfaceController
    {
        [HttpGet]
        public ActionResult Index(int id)
        {
            string cnContent = A8.CommonUmbraco.GetSiteSettingsContent(id).GetPropertyValue<string>("cnContent");
            if (cnContent.Length > 0)
            {
                return Content("<div class=\"magnifie-dialog\"><div class=\"paragraph\">" + cnContent + "</div></div>");
            }
            return Content("no content");
        }
    }
}