﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Umbraco.Core.Models;
using Umbraco.Web;
using Umbraco.Web.Models;

namespace A8
{
    public class CataloguePageController : PageBaseController
    {
        public override ActionResult Index(RenderModel model)
        {
            int detailId = A8.CommonUmbraco.GetCatalogueItemId(System.Web.HttpContext.Current.Request, model.Content.Url);
            if (detailId > 1)
            {
                // try render detail
                IPublishedContent detailPage = A8.CommonUmbraco.GetContentFromId(detailId);
                if (detailPage != null)
                {
                    // which catalogue type they want to prevent cross call eg: news -> event
                    IPublishedContent catalogueList = model.Content.GetPropertyValue<IPublishedContent>("catalogueList");

                    if (catalogueList.HasProperty("newsList") && detailPage.DocumentTypeAlias == "cat_OneNews")
                    {
                        // load detail only for few datas
                        A8.Catalogues.OneNews catalogueItem = A8.Catalogues.News.LoadOneNews(detailPage);
                        if (catalogueItem != null)
                        {
                            // cache part
                            var urlPath = ControllerContext.HttpContext.Request.Path;
                            ViewData["cache_page_id-" + urlPath] = detailId;
                            // end cache part

                            // og tags
                            string ogImage = string.Empty;
                            if (catalogueItem.Image != null)
                            {
                                ogImage = A8.CommonUmbraco.FormatOgImagePath(catalogueItem.Image);
                            }
                            return View("CatalogueItem", new PageBaseModel(model)
                            {
                                PageHtmlTitle = catalogueItem.Name,
                                MetaDescription = catalogueItem.Description,
                                MetaKeywords = catalogueItem.Keywords,
                                OgImage = ogImage,
                                CatalogueDetailId = detailPage.Id,
                                CatalogueDetailTypeAlias = detailPage.DocumentTypeAlias,
                                CatalogueDetailItem = catalogueItem
                            });
                        }
                    }

                    if (catalogueList.HasProperty("eventsList") && detailPage.DocumentTypeAlias == "cat_Event")
                    {
                        // load detail only for few datas
                        A8.Catalogues.Event catalogueItem = A8.Catalogues.Events.LoadEvent(detailPage);
                        if (catalogueItem != null)
                        {
                            // cache part
                            var urlPath = ControllerContext.HttpContext.Request.Path;
                            ViewData["cache_page_id-" + urlPath] = detailId;
                            // end cache part

                            // og tags
                            string ogImage = string.Empty;
                            if (catalogueItem.Image != null)
                            {
                                ogImage = A8.CommonUmbraco.FormatOgImagePath(catalogueItem.Image);
                            }
                            return View("CatalogueItem", new PageBaseModel(model)
                            {
                                PageHtmlTitle = catalogueItem.Name,
                                MetaDescription = catalogueItem.Description,
                                MetaKeywords = catalogueItem.Keywords,
                                OgImage = ogImage,
                                CatalogueDetailId = detailPage.Id,
                                CatalogueDetailTypeAlias = detailPage.DocumentTypeAlias,
                                CatalogueDetailItem = catalogueItem
                            });
                        }
                    }

                    // 404
                    A8.CommonUmbraco.Call404Page(model.Content);
                    return null;
                }
                else
                {
                    // 404
                    A8.CommonUmbraco.Call404Page(model.Content);
                    return null;
                }
            }
            return base.Index(new PageBaseModel(model));
        }
    }
}