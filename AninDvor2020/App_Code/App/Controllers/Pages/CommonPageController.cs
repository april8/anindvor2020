﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Umbraco.Web.Models;

namespace A8
{
    public class CommonPageController : PageBaseController
    {
        public override ActionResult Index(RenderModel model)
        {
            return base.Index(new PageBaseModel(model));
        }
    }
}