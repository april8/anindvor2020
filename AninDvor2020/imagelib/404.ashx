﻿<%@ WebHandler Language="C#" Class="_404" %>

using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Web;
using System.Web.Services;
using System.Web.Services.Protocols;
using System.Drawing;

public class _404 : IHttpHandler
{
    public void ProcessRequest(HttpContext context)
    {
        string query;

        if (context.Request.QueryString["aspxerrorpath"] != null)
        {
            query = context.Request.QueryString["aspxerrorpath"];
        }
        else
        {
            query = context.Request.Url.OriginalString.ToString();
            if (query.IndexOf("404;") >= 0)
            {
                query = query.Substring(query.IndexOf("404;") + 4);
                query = query.Substring(query.IndexOf("/imagelib/") + 10);
                string error = string.Empty;
                A8.Imagelib il = new A8.Imagelib();
                bool result = il.ProcessQuery(query, ref error);
                if (result)
                {
                    context.Response.Redirect("~/imagelib/" + query);
                }
                else
                {
                    // spacer.gif
                    byte[] gif = {
                        0x47, 0x49, 0x46, 0x38, 0x39, 0x61, 0x01, 0x00, 0x01, 0x00, 0x91, 0x00, 0x00, 0xFF, 0xFF, 0xFF,
                        0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x21, 0xF9, 0x04, 0x09, 0x00, 0x00, 0x00,
                        0x00, 0x2C, 0x00, 0x00, 0x00, 0x00, 0x01, 0x00, 0x01, 0x00, 0x00, 0x08, 0x04, 0x00, 0x01, 0x04
                        };

                    context.Response.ContentType = "image/gif";
                    context.Response.BinaryWrite(gif);
                    context.Response.End();
                }
            }
            else
            {
                context.Response.End();
            }
        }
    }

    public bool IsReusable
    {
        get { return true; }
    }
}