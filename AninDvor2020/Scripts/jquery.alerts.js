(function ($) {
    $.alerts = {
        // public properties
        defaults: {
            okButton: '&nbsp;OK&nbsp;',				// text for the OK button
            cancelButton: '&nbsp;Cancel&nbsp;',		// text for the Cancel button
            customClass: null,						// apply custom css
            autoClose: null							// auto-close dialog after x seconds
        },

        settings: null,

        // private properties
        _t: null,								// timer identifier

        // public methods
        info: function (message, callback, options) {
            this.settings = $.extend({}, this.defaults, options);
            this._show(message, 'info', function (result) {
                if (callback) callback(result);
            });
        },
        alert: function (message, callback, options) {
            this.settings = $.extend({}, this.defaults, options);
            this._show(message, 'alert', function (result) {
                if (callback) callback(result);
            });
        },
        confirm: function (message, callback, options) {
            this.settings = $.extend({}, this.defaults, options);
            this._show(message, 'confirm', function (result) {
                if (callback) callback(result);
            });
        },

        // private methods
        _show: function (msg, type, callback) {
            $.alerts._hide();
            $.alerts._overlay('show');
            $("BODY").append(
                '<div id="dialogContainer">' +
                '<div id="dialogContent">' +
                '<div id="dialogMessage"></div>' +
                '</div>' +
                '</div>');
            if ($.alerts.settings.customClass) $("#dialogContainer").addClass($.alerts.settings.customClass);

            $("#dialogContainer").css({
                position: 'fixed',
                zIndex: 99999,
                margin: 0
            });
            $("#dialogContent").addClass(type);
            $("#dialogMessage").text(msg);
            $("#dialogMessage").html($("#dialogMessage").text().replace(/\n/g, '<br />'));
            $("#dialogContainer").css({
                minWidth: 300 + "px",
                maxWidth: $("#dialogContainer").outerWidth() + "px"
            });
            $.alerts._reposition();
            $(window).bind('resize', $.alerts._reposition);
            switch (type) {
                case 'info':
                    $("#dialogMessage").after('<div id="dialogButtons"><input class="btn btn-primary" type="button" value="' + $.alerts.settings.okButton + '" id="dialogOk" /></div>');
                    $("#dialogOk").click(function () {
                        $.alerts._hide();
                        callback(true);
                    });
                    $("#dialogOk").focus().keyup(function (e) {
                        if (e.keyCode == 13 || e.keyCode == 27) $("#dialogOk").trigger('click');
                    });
                    break;
                case 'alert':
                    $("#dialogMessage").after('<div id="dialogButtons"><input class="btn btn-primary" type="button" value="' + $.alerts.settings.okButton + '" id="dialogOk" /></div>');
                    $("#dialogOk").click(function () {
                        $.alerts._hide();
                        callback(true);
                    });
                    $("#dialogOk").focus().keyup(function (e) {
                        if (e.keyCode == 13 || e.keyCode == 27) $("#dialogOk").trigger('click');
                    });
                    break;
                case 'confirm':
                    $("#dialogMessage").after('<div id="dialogButtons"><input class="btn btn-primary" type="button" value="' + $.alerts.settings.okButton + '" id="dialogOk" />&nbsp;<input type="button" class="btn btn-primary" value="' + $.alerts.settings.cancelButton + '" id="dialogCancel" /></div>');
                    $("#dialogOk").click(function () {
                        $.alerts._hide();
                        if (callback) callback(true);
                    });
                    $("#dialogCancel").click(function () {
                        $.alerts._hide();
                        if (callback) callback(false);
                    });
                    $("#dialogOk").focus();
                    $("#dialogOk, #dialogCancel").keyup(function (e) {
                        if (e.keyCode == 13) $("#dialogOk").trigger('click');
                        if (e.keyCode == 27) $("#dialogCancel").trigger('click');
                    });
                    break;
            }
            $("#dialogButtons").css({ textAlign: 'center' });
            if ($.alerts.settings.customClass) { $.alerts.settings.customClass = null; }
            if ($.alerts.settings.autoClose) {
                $.alerts._t = setTimeout('$.alerts._hide();', $.alerts.settings.autoClose);
                $.alerts.settings.autoClose = null;
            }
        },

        _hide: function () {
            $("#dialogContainer").remove();
            $.alerts._overlay('hide');
            $(window).unbind('resize', $.alerts._reposition);
            if ($.alerts._t) clearTimeout($.alerts._t);
        },

        _overlay: function (status) {
            switch (status) {
                case 'show':
                    $.alerts._overlay('hide');
                    //$.alerts._fixScroll();
                    $("BODY").append('<div id="dialogMask" tabindex="0"></div>');
                    $("#dialogMask").css({
                        position: 'absolute',
                        zIndex: 99998,
                        top: '0px',
                        left: '0px',
                        width: '100%',
                        height: $(document).height() + "px",
                        opacity: '0' // default 0.4-0.8
                    });
                    break;
                case 'hide':
                    //$.alerts._unfixScroll();
                    $("#dialogMask").remove();
                    break;
            }
        },

        _reposition: function () {
            $.alerts._overlay('hide');
            var top = (($(window).height() / 2) - ($("#dialogContainer").outerHeight() / 2)) - 75;
            var left = (($(window).width() / 2) - ($("#dialogContainer").outerWidth() / 2));
            if (top < 0) top = 0;
            if (left < 0) left = 0;
            $("#dialogContainer").css({
                top: top + 'px',
                left: left + 'px'
            });
            $("#dialogMask").height($(document).height());
            $.alerts._overlay('show');
        },

        // when html tag became fixed, then scrollbar disappeared, what caused screen to flicker
        // this function recalculate scrollbar width and fix right margin accordingly
        _fixScroll: function () {
            var marginRightPx = 0;
            if (window.getComputedStyle) {
                var bodyStyle = window.getComputedStyle(document.body);
                if (bodyStyle) {
                    marginRightPx = parseInt(bodyStyle.marginRight, 10);
                }
            }
            var scrollbarWidthPx = window.innerWidth - document.body.clientWidth;

              // customization!
            $("body").css("width", 'calc(100% - ' + scrollbarWidthPx + 'px)');
            $("body").css("overflow", "hidden");
            //Object.assign(document.body.style, {
            //    //width: 'auto',
            //    //overflow: 'hidden',
            //    //marginRight: marginRightPx + scrollbarWidthPx + "px"
            //    width: 'calc(100% - ' + scrollbarWidthPx + 'px)',
            //    overflow: 'hidden'
            //});
        },

        _unfixScroll: function () {
            var marginRightPx = 0;

             // customization! 
            $("body").css("width", "");
            $("body").css("overflow", "");
            /*
            Object.assign(document.body.style, {
                //width: '100%',
                //overflow: 'auto',
                //marginRight: marginRightPx + "px"
                width: 'initial',
                overflow: 'initial'
            });*/
        }

    }

    // shortuct functions
    jInfo = function (message, callback, options) { $.alerts.info(message, callback, options); }
    jAlert = function (message, callback, options) { $.alerts.alert(message, callback, options); }
    jConfirm = function (message, callback, options) { $.alerts.confirm(message, callback, options); };

})(jQuery);


/*
// usage
document.addEventListener('DOMContentLoaded', function () {
    jConfirm("Confirm", function (callback) {
        if (callback) {
            jInfo("Confirmed");
        }
        else {
            jAlert("Not confirmed");
        }
    });
});

regular href confirm simulation - oneliner
onclick="(function(e){ jConfirm('Confirm!', function(callback) { if(callback) { window.location = e.target; } }); e.preventDefault(); })(event)"
 */