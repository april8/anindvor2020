﻿document.addEventListener('DOMContentLoaded', function () {


});

function ShowLoadingOverlay() {
    $(".loading-overlay").show();
    $("body").addClass("no-scroll");
}

function HideLoadingOverlay() {
    $(".loading-overlay").hide();
    $("body").removeClass("no-scroll");
}

function IsEmail(mail) {
    if (/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,4})+$/.test(mail)) {
        return (true);
    }
    return (false);
}

function SessionSet(key, value, callback) {
    $.post('/Session/SessionSet',
        { k: key, v: value }, function (result) {
            callback(result.value);
        });
}

function SessionGet(key, callback) {
    $.get('/Session/SessionGet',
        { k: key }, function (result) {
            callback(result.value);
        });
}

function debounce(func, wait, immediate) {
    var timeout;
    return function () {
        var context = this,
            args = arguments;
        var later = function () {
            timeout = null;
            if (!immediate) {
                func.apply(context, args);
            }
        };
        var callNow = immediate && !timeout;
        clearTimeout(timeout);
        timeout = setTimeout(later, wait);
        if (callNow) {
            func.apply(context, args);
        }
    };
}

document.addEventListener('DOMContentLoaded', function () {
    $('.magnify-image').magnificPopup({
        type: 'image',
        preloader: false
    });

    $('.magnify-video, .magnify-gmap').magnificPopup({
        type: 'iframe',
        disableOn: 700,
        //mainClass: 'mfp-custom',
        removalDelay: 160,
        preloader: false,
        fixedContentPos: false
    });


    $('.magnify-gallery').magnificPopup({
        delegate: 'a',
        type: 'image',
        tLoading: 'Loading image #%curr%...',
        //mainClass: 'mfp-img-mobile',
        gallery: {
            enabled: true,
            navigateByImgClick: true,
            preload: [0, 1] // Will preload 0 - before current, and 1 after the current image
        },
        image: {
            tError: '<a href="%url%">The image #%curr%</a> could not be loaded.',
            titleSrc: function (item) {
                //return item.el.attr('title') + '<small>&copy; April8</small>';
            }
        }
    });


});

/////
// Change class to your content outer container.
//---
//var fullContainer = $('body');

// Stretch to the the full width
var stretchFullWidth = function () {
    var $panelsRow = $('.r-section--full-width');
    $panelsRow.each(function () {
        var $$ = $(this);

        // Reset all the styles associated with row stretching
        $$.css({
            'margin-left': 0,
            'margin-right': 0,
            'padding-left': 0,
            'padding-right': 0
        });

        var leftSpace = $$.offset().left - fullContainer.offset().left,
            rightSpace = fullContainer.outerWidth() - leftSpace - $$.parent().outerWidth();

        $$.css({
            'margin-left': - leftSpace,
            'margin-right': - rightSpace,
            'padding-left': leftSpace,
            'padding-right': rightSpace
        });
    });
}
$(window).on('resize load', stretchFullWidth);
stretchFullWidth();

// Stretch to the full width with content
var stretchFullWidthStretched = function () {
    var $panelsRow = $('.r-section--full-width-stretched');
    $panelsRow.each(function () {
        var $$ = $(this);

        // Reset all the styles associated with row stretching
        $$.css({
            'margin-left': 0,
            'margin-right': 0,
            'padding-left': 0,
            'padding-right': 0
        });

        var leftSpace = $$.offset().left - fullContainer.offset().left,
            rightSpace = fullContainer.outerWidth() - leftSpace - $$.parent().outerWidth();

        $$.css({
            'margin-left': - leftSpace,
            'margin-right': - rightSpace,
            'padding-left': 0,
            'padding-right': 0
        });
    });
}
$(window).on('resize load', stretchFullWidthStretched);
stretchFullWidthStretched();
//---
/////


// custom gallery slider plugin usin slick.js
(function ($) {
    $.fn.gallerySlider = function (options) {
        var defaults = {
            slidesToShow: 1
        };
        var settings = $.extend({}, defaults, options);
        if (this.length > 1) {
            this.each(function () {
                $(this).pluginName(options);
            });
            return this;
        }

        return this.each(function () {
            var base = $(this);

            base.slick({
                lazyLoad: 'ondemand',
                slidesToShow: settings.slidesToShow
            });

            // start playing video when slide is active
            base.on('beforeChange', function (event, slick, currentSlide, nextSlide) {
                var src = $(slick.$slider).find(".slick-current iframe");
                if (src.length) {
                    setTimeout(function () {
                        src[0].contentWindow.postMessage('{"event":"command","func":"' + 'pauseVideo' + '","args":""}', '*');
                    }, 200);
                }
            });

            // stop playing video when slide is not active
            base.on('afterChange', function (event, slick, currentSlide, nextSlide) {
                var src = $(slick.$slider).find(".slick-current iframe");
                if (src.length) {
                    setTimeout(function () {
                        src[0].contentWindow.postMessage('{"event":"command","func":"' + 'playVideo' + '","args":""}', '*');
                    }, 200);
                }
            });

        });
    };
})(jQuery);

// convert plain email text to clickable link ---
// just add this class to element for which
// you want conversion, or add elements here
$(function () {
    $(".email, .email-convert").filter(function () {
        var html = $(this).html();
        var emailPattern = /[a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}/g;

        var matched_str = $(this).html().match(emailPattern);
        if (matched_str) {
            var text = $(this).html();
            $.each(matched_str, function (index, value) {
                text = text.replace(value, "<a href='mailto:" + value + "'>" + value + "</a>");
            });
            $(this).html(text);
            return $(this);
        }
    });
});
// convert plain email text to clickable link ---

// Tabs
(function ($) {
    $.fn.tabs = function (options) {
        var defaults = {
            active_tab: 0
        };
        var settings = $.extend({}, defaults, options);
        if (this.length > 1) {
            this.each(function () { $(this).pluginName(options) });
            return this;
        }

        return this.each(function () {
            var base = $(this);

            // all
            $('.r-tabs__content', base).hide();

            // this
            $('.r-tabs__title', base).click(function () {
                $('.r-tabs__content', base).hide();
                var activeTab = $(this).attr('rel');
                $('.' + activeTab, base).fadeIn();
                $('.r-tabs__title', base).removeClass('r-tabs__title--active');
                $(this).addClass('r-tabs__title--active');
            });

            var hsh = window.location.hash || '';
            if (hsh.length > 0) hsh = hsh.replace('#', '');
            var hshTriggered = false;
            if (hsh.length > 0) {
                var $hash = base.find(".r-tabs__title[data-hash='" + hsh + "']") || [];
                if ($hash.length > 0) {
                    $hash.get(0).click();
                }
            }
            else {
                $('.r-tabs__title', base).eq(settings.active_tab).click();
            }
        });
    };
})(jQuery);

// Collapsible
(function ($) {
    $.fn.collapsible = function (options) {
        var defaults = {
            preserve_state: false,
            active_block: null
        };
        var settings = $.extend({}, defaults, options);
        if (this.length > 1) {
            this.each(function () { $(this).pluginName(options) });
            return this;
        }

        return this.each(function () {
            var base = $(this);

            $('.r-collapsible__title', base).click(function () {
                $this = $(this);
                if (settings.preserve_state) {
                    if ($this.hasClass('r-collapsible__title--opened')) {
                        $this.next('.r-collapsible__content', base).slideUp('fast');
                        $this.removeClass('r-collapsible__title--opened');
                        $this.addClass('r-collapsible__title--closed');
                    }
                    else {
                        // all
                        $('.r-collapsible__content', base).slideUp('fast');
                        $('.r-collapsible__title', base).removeClass('r-collapsible__title--opened');
                        $('.r-collapsible__title', base).addClass('r-collapsible__title--closed');

                        // this
                        $this.next('.r-collapsible__content').slideToggle('fast');
                        $this.toggleClass('r-collapsible__title--closed');
                        $this.toggleClass('r-collapsible__title--opened');
                    }
                }
                else {
                    $this.next('.r-collapsible__content').slideToggle('fast');
                    $this.toggleClass('r-collapsible__title--closed');
                    $this.toggleClass('r-collapsible__title--opened');
                }
            });

            var hsh = window.location.hash || '';
            if (hsh.length > 0) hsh = hsh.replace('#', '');
            if (hsh.length > 0) {
                var $hash = base.find(".r-collapsible__title[data-hash='" + hsh + "']") || [];
                if ($hash.length > 0) {
                    $hash.get(0).click();
                }
            }
            else {
                if (settings.active_block != null) {
                    $('.r-collapsible__title', base).eq(settings.active_block).click();
                }
            }
        });
    };
})(jQuery);

/* cookie notification trigger */
function OpenCn(pageId) {
    $.get("/umbraco/surface/CookieConsent/Index/" + pageId, function (data, status) {
        if (status == "success") {
            $.magnificPopup.open({
                items: {
                    src: data,
                    type: 'inline'
                }
            });
        }
    });
}
/* cookie notification trigger */

/* Decorate links with custom classes for GTM */
document.addEventListener('DOMContentLoaded', function (i) {
    var DecorateLinks = function () {
        function _decorate(o, _type, _target) {
            var _targetType = "";
            switch (_target) {
                case "_blank":
                    {
                        _targetType = "-outer";
                        break;
                    }
            }
            if (!o.hasClass("link-" + _type + _targetType)) {
                o.addClass("link-" + _type + _targetType);
            }
        }
        function _init() {
            $("a").each(function () {
                var $this = $(this);
                var href = ($this.attr("href") || "").toLowerCase();
                var target = ($this.attr("target") || "").toLowerCase();
                if (href.indexOf("http") > -1) {
                    _decorate($this, "link", target);
                };
                if (href.indexOf("mailto") > -1) {
                    _decorate($this, "mail", target);
                }
                if (href.indexOf("tel") > -1) {
                    _decorate($this, "phone", target);
                }
                if (href.indexOf("ftp") > -1) {
                    _decorate($this, "ftp", target);
                }
            });
        }
        return _init();
    };
    DecorateLinks();
});
/* --- */

/* Deadly simple toast */
var Toast = {
    _msg: "Toast message",
    _color: "white",
    _bgColor: "#5cb85c", //"green",
    _t: $("._toast") || [],
    _hide: function (_tc) {
        var _this = this;
        if (_this._t.length > 0) {
            _this._t.fadeOut(500, function () {
                _this._t.remove();
                _this._t = [];
                clearTimeout(window['_toastCounter_' + _tc]);
            });
        }
    },
    _show: function (_tc) {
        var _this = this;
        _this._t.fadeIn(500);
        window['_toastCounter_' + _tc] = setTimeout(function () {
            _this._hide();
        }, 3500);
    },
    show: function (params) {
        params = params || [];
        var msg = params.msg || "";
        var color = params.color || "";
        var bgColor = params.bgColor || "";
        if (this._t.length == 0) {
            var _tc = Date.now();
            $("body").append("<div onclick='Toast._hide(" + _tc + ");' title='×' class='_toast' style='color:" + (color.length > 0 ? color : this._color) + ";background-color:" + (bgColor.length > 0 ? bgColor : this._bgColor) + ";'>" + (msg.length > 0 ? msg : this._msg) + "</div>");
            this._t = $("._toast");
            this._show(_tc);
        }
    }
};
/* --- */

$.fn.removeClassStartingWith = function (filter) {
    $(this).removeClass(function (index, className) {
        return (className.match(new RegExp("\\S*" + filter + "\\S*", 'g')) || []).join(' ');
    });
    return this;
};

/*
 * Maintain & keep scroll position after post-back & postback & refresh.
 * Just include this js file (no need for cookies).
 *
 * Author: Evalds Urtans
 * Website: http://www.evalds.lv
 */

//document.addEventListener('DOMContentLoaded', function () {
//    var sep = '\uE000'; // an unusual char: unicode 'Private Use, First'

//    window.addEventListener('pagehide', function (e) {
//        window.name += sep + window.pageXOffset + sep + window.pageYOffset;
//    });

//    if (window.name && window.name.indexOf(sep) > -1) {
//        var parts = window.name.split(sep);
//        if (parts.length >= 3) {
//            window.name = parts[0];
//            window.scrollTo(parseFloat(parts[parts.length - 2]), parseFloat(parts[parts.length - 1]));
//        }
//    }
//});

/* responsive background images. KEEP THIS FUNCTION LAST */
(function () {
    function setRbi($this, attempt, a) {
        if (attempt <= 50) {
            attempt++;

            var $img = $this.find("img");
            var src = typeof $img.prop("currentSrc") !== 'undefined' ? $img.prop("currentSrc") : $img.attr("data-src");

            if (src && src.length > 0) {

                $this.css("background-image", "url('" + src + "')");
                if (a) {
                    $this.addClass("visibile");
                }

                return;
            }
            else {
                setTimeout(function () {
                    setRbi($this, attempt, a);
                }, 100);
            }
        }
    }

    function ResponsiveBackgroundImage(a) {
        setTimeout(function () {
            $(".rbi").each(function () {
                setRbi($(this), 0, a);
            });
        });
    }

    $(function () {
        ResponsiveBackgroundImage(true);
    });

    window.onresize = debounce(function () {
        ResponsiveBackgroundImage(false);
    }, 500);

})();
